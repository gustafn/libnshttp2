# NaviServer HTTP/2 library

HTTP/2 C functions to handle the following:

* Framing layer - functions/structures to serialize and deserialize HTTP/2 frames from/to a stream.
* Stream layer - functions/structures to handle stream states, multiplex/demultiplex HTTP/2 messages.
* Frame list - functions/structures to deserialize and serializes a list of frames from and into a buffer.
* Header compression/decompression (HPACK) - functions/structures to encode and decode HTTP/2 headers using HPACK and
  nghttp2.
* HTTP/2 connection management

## Prerequisites

You must have the following libraries and software:

* At least a C99 compatible compiler. Tested currently: GCC (Unix) and MinGW (Windows).
* Cmake 3.9+
* nghttp2

## Install/Package

How to install?

If you wish to use your own library directories use:

* `-DNGHTTP2_LIB_PATH=<library_directory>` for nghttp2

If you want to use C90 instead of the default C99, specify `-DCMAKE_C_STANDARD=90`

### Linux

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
sudo make install
```

### Windows

1. Install [MSYS2](https://www.msys2.org/).
2. install the packages MinGW, CMake, nghttp2 using MSYS2:

```
pacman -S mingw-w64-x86_64-cmake mingw-w64-x86_64-gcc mingw-w64 mingw-w64-x86_64-nghttp2
```

3. Make sure that cmake and mingw tools are in the path of the current user or system. These can be found per default
   in `C:\msys64\mingw64\bin`.
4. In order to build the files create a build directory open it in cmd or PowerShell and run:

```
cmake -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - MinGW Makefiles" ..
cmake -DCMAKE_BUILD_TYPE=Release --build .
```

* Visual Studio C++

TODO, currently not automated and not supported

## Usage

You can use the library as a shared C library in your project.

You can use your own memory allocators by including the `<nshttp2/memory.h>` headers and using
the `http2SetupMemoryAllocators` function.

## Contributing

PRs accepted.

## License

MIT © Filip Minic