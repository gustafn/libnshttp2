/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "stream.h"
#include <nshttp2/stream.h>
#include <nshttp2/unity/unity.h>
#include <nshttp2/utility/nghttp2.h>

void test_http2_stream_allocate(void) {
    Http2StreamT *stream;
    http2StreamAllocate(&stream,
                        4u,
                        255u,
                        65000u,
                        64000u,
                        10u * 1024u * 1024u);

    TEST_ASSERT_NOT_NULL(stream);
    TEST_ASSERT_EQUAL(4u, stream->streamId);
    TEST_ASSERT_EQUAL(255u, stream->weight);
    TEST_ASSERT_EQUAL(65000u, stream->remainingSendWindow);
    TEST_ASSERT_EQUAL(64000u, stream->remainingReceiveWindow);
    TEST_ASSERT_EQUAL(IDLE, stream->state);
    TEST_ASSERT_NULL(stream->parent);
    TEST_ASSERT_NULL(stream->children);
    TEST_ASSERT_NULL(stream->siblings);
    TEST_ASSERT_EQUAL(0u, stream->flags);

    http2StreamDeallocate(stream);
}

void test_http2_stream_deallocate(void) {
    Http2StreamT *stream;
    http2StreamAllocate(&stream,
                        4u,
                        255u,
                        65000u,
                        65000u,
                        10u * 1024u * 1024u);

    http2StreamDeallocate(stream);
}

void test_http2_stream_add_child(void) {
    Http2StreamT *second_stream;
    Http2StreamT *second_stream_ptr = NULL;
    Http2StreamT *first_stream;

    http2StreamAllocate(&first_stream,
                        4u,
                        255u,
                        65000u,
                        65000u,
                        10u * 1024u * 1024u);

    http2StreamAllocate(&second_stream,
                        6u,
                        230u,
                        53000u,
                        65000u,
                        10u * 1024u * 1024u);

    http2StreamAddChild(first_stream, second_stream);

    TEST_ASSERT_NOT_NULL(first_stream->children);

    streamHashmapGet(first_stream->children, second_stream->streamId, &second_stream_ptr);

    TEST_ASSERT_EQUAL(second_stream, second_stream_ptr);
    TEST_ASSERT_EQUAL(second_stream->parent, first_stream);

    http2StreamDeallocate(first_stream);
    http2StreamDeallocate(second_stream);
}

void test_http2_stream_remove_child(void) {
    Http2StreamT *first_stream;
    Http2StreamT *second_stream;
    Http2StreamT *third_stream;
    Http2StreamT *second_stream_ptr;
    Http2StreamT *third_stream_ptr;

    http2StreamAllocate(&first_stream,
                        4u,
                        255u,
                        65000u,
                        53000u,
                        10u * 1024u * 1024u);

    http2StreamAllocate(&second_stream,
                        6u,
                        230u,
                        53000u,
                        65000u,
                        10u * 1024u * 1024u);

    http2StreamAllocate(&third_stream,
                        7u,
                        230u,
                        29999u,
                        65000u,
                        10u * 1024u * 1024u);

    http2StreamAddChild(first_stream, second_stream);
    http2StreamAddChild(first_stream, third_stream);

    TEST_ASSERT_NOT_NULL(first_stream->children);

    streamHashmapGet(first_stream->children, second_stream->streamId, &second_stream_ptr);

    http2StreamRemoveChild(first_stream, third_stream);

    streamHashmapGet(first_stream->children, third_stream->streamId, &third_stream_ptr);

    TEST_ASSERT_NULL(third_stream_ptr);
    TEST_ASSERT_EQUAL(second_stream, second_stream_ptr);
    TEST_ASSERT_EQUAL(second_stream->parent, first_stream);

    http2StreamDeallocate(first_stream);
    http2StreamDeallocate(second_stream);
    http2StreamDeallocate(third_stream);
}

void test_http2_stream_add_sibling(void) {
    Http2StreamT *first_stream;
    Http2StreamT *second_stream;
    Http2StreamT *retrieved_stream;

    http2StreamAllocate(&first_stream,
                        4u,
                        255u,
                        65000u,
                        65000u,
                        10u * 1024u * 1024u);

    http2StreamAllocate(&second_stream,
                        6u,
                        230u,
                        53000u,
                        65000u,
                        10u * 1024u * 1024u);

    http2StreamAddSibling(first_stream, second_stream);

    TEST_ASSERT_NOT_NULL(first_stream->siblings);
    TEST_ASSERT_NOT_NULL(second_stream->siblings);

    streamHashmapGet(first_stream->siblings, second_stream->streamId, &retrieved_stream);

    TEST_ASSERT_EQUAL(second_stream, retrieved_stream);

    streamHashmapGet(second_stream->siblings, first_stream->streamId, &retrieved_stream);

    TEST_ASSERT_EQUAL(first_stream, retrieved_stream);

    http2StreamDeallocate(first_stream);
    http2StreamDeallocate(second_stream);
}

void test_http2_stream_remove_sibling(void) {
    Http2StreamT *first_stream;
    Http2StreamT *second_stream;
    Http2StreamT *second_stream_ptr;

    http2StreamAllocate(&first_stream,
                        4u,
                        255u,
                        65000u,
                        65000u,
                        10u * 1024u * 1024u);

    http2StreamAllocate(&second_stream,
                        6u,
                        230u,
                        53000u,
                        65000u,
                        10u * 1024u * 1024u);

    http2StreamAddSibling(first_stream, second_stream);
    http2StreamRemoveSibling(first_stream, second_stream);

    streamHashmapGet(second_stream->siblings, first_stream->streamId, &second_stream_ptr);

    TEST_ASSERT_NULL(second_stream_ptr);

    streamHashmapGet(first_stream->siblings, second_stream->streamId, &second_stream_ptr);

    TEST_ASSERT_NULL(second_stream_ptr);

    TEST_ASSERT_NULL(first_stream->siblings);
    TEST_ASSERT_NULL(second_stream->siblings);

    http2StreamDeallocate(first_stream);
    http2StreamDeallocate(second_stream);
}

void test_http2_stream_set_state(void) {
    Http2StreamT *stream;
    http2StreamAllocate(&stream, 3u, 200u, 54000u, 5300u, 10u * 1024u * 1024u);

    TEST_ASSERT_EQUAL(stream->state, IDLE);

    http2StreamSetState(stream, OPEN);

    TEST_ASSERT_EQUAL(stream->state, OPEN);

    http2StreamSetState(stream, HALF_CLOSED_REMOTE);

    TEST_ASSERT_EQUAL(stream->state, HALF_CLOSED_REMOTE);

    http2StreamDeallocate(stream);
}

void test_http2_stream_frame_get_error(void) {
    Http2StreamT *stream;
    uint_fast8_t i;
    Http2FrameT frame;
    uint8_t valid_frames[2] = {HEADERS_FRAME_TYPE, PUSH_PROMISE_FRAME_TYPE};
    uint8_t invalid_frames[4] = {RST_STREAM_FRAME_TYPE, CONTINUATION_FRAME_TYPE, DATA_FRAME_TYPE, 255u};

    http2StreamAllocate(&stream, 3u, 20u, 12000u, 13000u, 10u * 1024u * 1024u);

    frame.header.streamId = 3u;
    frame.header.length = 0u;
    frame.header.flags = 0u;
    frame.header.type = WINDOW_UPDATE_FRAME_TYPE;

    for (i = 0; i < 2u; ++i) {
        frame.header.type = valid_frames[i];
        TEST_ASSERT_EQUAL(NO_ERROR_ERROR_CODE, http2StreamFrameGetError(stream, &frame, false));
    }


    for (i = 0; i < 3u; ++i) {
        frame.header.type = invalid_frames[i];
        TEST_ASSERT_EQUAL(PROTOCOL_ERROR_ERROR_CODE, http2StreamFrameGetError(stream, &frame, false));
    }

    http2StreamSetState(stream, RESERVED_LOCAL);

    valid_frames[1] = RST_STREAM_FRAME_TYPE;

    for (i = 0; i < 2u; ++i) {
        frame.header.type = valid_frames[i];
        TEST_ASSERT_EQUAL(NO_ERROR_ERROR_CODE, http2StreamFrameGetError(stream, &frame, true));
    }

    invalid_frames[0] = PUSH_PROMISE_FRAME_TYPE;

    for (i = 0; i < 3u; ++i) {
        frame.header.type = invalid_frames[i];
        TEST_ASSERT_EQUAL(PROTOCOL_ERROR_ERROR_CODE, http2StreamFrameGetError(stream, &frame, true));
    }

    frame.header.type = RST_STREAM_FRAME_TYPE;
    TEST_ASSERT_EQUAL(NO_ERROR_ERROR_CODE, http2StreamFrameGetError(stream, &frame, false));

    /* TODO test all cases */

    http2StreamDeallocate(stream);
}

void test_http2_stream_add_header_block(void) {
    Http2StreamT *stream;
    Http2HeaderFieldT first_field;
    Http2HeaderFieldT second_field;
    Http2HeaderListElementT second_element;
    Http2HeaderListElementT first_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater;
    nghttp2_hd_inflater *nghttp2HdInflater;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT headers_payload;
    Http2FrameT headers_frame;
    Http2ContinuationPayloadT continuation_payload;
    Http2FrameT continuation_frame;
    Http2HeaderListT *new_header_list;

    http2StreamAllocate(&stream, 3u, 20u, 65000u, 65000u, 10u * 1024u * 1024u);

    first_field.name = (uint8_t *) "user-agent";
    first_field.nameLength = strlen("user-agent");
    first_field.value = (uint8_t *) "test";
    first_field.valueLength = strlen("test");

    second_field.name = (uint8_t *) "user-agent";
    second_field.nameLength = strlen("user-agent");
    second_field.value = (uint8_t *) "kjskdjkadjksajdksajdowieuqowhroiqwhrtowqhotgohdsogodsofhosdhfosdf";
    second_field.valueLength = strlen("kjskdjkadjksajdksajdowieuqowhroiqwhrtowqhotgohdsogodsofhosdhfosdf");

    second_element.http2HeaderField = &second_field;
    second_element.next = NULL;

    first_element.http2HeaderField = &first_field;
    first_element.next = &second_element;

    header_list.head = &first_element;
    header_list.tail = &second_element;
    header_list.length = 2u;

    nghttp2HdDeflater = NULL;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    nghttp2HdInflater = NULL;

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    headers_payload.headerPayload = buffer->buffer;

    headers_frame.header.type = HEADERS_FRAME_TYPE;
    headers_frame.header.flags = DEFAULT_FRAME_FLAG;
    headers_frame.header.length = 2u;
    headers_frame.header.streamId = 3u;
    headers_frame.payload.http2HeadersFramePayload = &headers_payload;

    http2StreamAddHeaderBlock(stream, &headers_frame);

    continuation_payload.headerBlockFragment = buffer->buffer + 2;

    continuation_frame.header.type = CONTINUATION_FRAME_TYPE;
    continuation_frame.header.flags = END_HEADERS_FRAME_FLAG;
    continuation_frame.header.length = (uint32_t) buffer->length - 2u;
    continuation_frame.header.streamId = 3u;
    continuation_frame.payload.http2ContinuationPayload = &continuation_payload;

    http2StreamAddHeaderBlock(stream, &continuation_frame);

    http2StreamDecodeHeaders(stream, &new_header_list, nghttp2HdInflater);

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_header_list->head->http2HeaderField->name,
                             header_list.head->http2HeaderField->name,
                             header_list.head->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_header_list->head->http2HeaderField->value,
                             header_list.head->http2HeaderField->value,
                             header_list.head->http2HeaderField->valueLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_header_list->tail->http2HeaderField->name,
                             header_list.tail->http2HeaderField->name,
                             header_list.tail->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_header_list->tail->http2HeaderField->value,
                             header_list.tail->http2HeaderField->value,
                             header_list.tail->http2HeaderField->valueLength));

    http2StreamDeallocate(stream);
    http2HeaderListDeallocate(new_header_list);
    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
}

void test_http2_stream_decode_headers(void) {
    Http2StreamT *stream;
    Http2HeaderFieldT first_field;
    Http2HeaderFieldT second_field;
    Http2HeaderListElementT second_element;
    Http2HeaderListElementT first_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater;
    nghttp2_hd_inflater *nghttp2HdInflater;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT headers_payload;
    Http2FrameT headers_frame;
    Http2ContinuationPayloadT continuation_payload;
    Http2FrameT continuation_frame;
    Http2HeaderListT *new_header_list;

    http2StreamAllocate(&stream,
                        3u,
                        20u,
                        65000u,
                        65000u,
                        10u * 1024u * 1024u);

    first_field.name = (uint8_t *) "user-agent";
    first_field.nameLength = strlen("user-agent");
    first_field.value = (uint8_t *) "test";
    first_field.valueLength = strlen("test");

    second_field.name = (uint8_t *) "user-agent";
    second_field.nameLength = strlen("user-agent");
    second_field.value = (uint8_t *) "kjskdjkadjksajdksajdowieuqowhroiqwhrtowqhotgohdsogodsofhosdhfosdf";
    second_field.valueLength = strlen("kjskdjkadjksajdksajdowieuqowhroiqwhrtowqhotgohdsogodsofhosdhfosdf");

    second_element.http2HeaderField = &second_field;
    second_element.next = NULL;

    first_element.http2HeaderField = &first_field;
    first_element.next = &second_element;

    header_list.head = &first_element;
    header_list.tail = &second_element;
    header_list.length = 2u;

    nghttp2HdDeflater = NULL;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    nghttp2HdInflater = NULL;

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    headers_payload.headerPayload = buffer->buffer;
    headers_frame.header.type = HEADERS_FRAME_TYPE;
    headers_frame.header.flags = DEFAULT_FRAME_FLAG;
    headers_frame.header.streamId = 3u;
    headers_frame.header.length = 2u;
    headers_frame.payload.http2HeadersFramePayload = &headers_payload;

    http2StreamAddHeaderBlock(stream, &headers_frame);

    continuation_payload.headerBlockFragment = buffer->buffer + 2;

    continuation_frame.header.type = CONTINUATION_FRAME_TYPE;
    continuation_frame.header.flags = END_HEADERS_FRAME_FLAG;
    continuation_frame.header.length = buffer->length - 2u;
    continuation_frame.header.streamId = 3u;
    continuation_frame.payload.http2ContinuationPayload = &continuation_payload;

    http2StreamAddHeaderBlock(stream, &continuation_frame);

    http2StreamDecodeHeaders(stream, &new_header_list, nghttp2HdInflater);

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_header_list->head->http2HeaderField->name,
                             header_list.head->http2HeaderField->name,
                             header_list.head->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_header_list->head->http2HeaderField->value,
                             header_list.head->http2HeaderField->value,
                             header_list.head->http2HeaderField->valueLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_header_list->tail->http2HeaderField->name,
                             header_list.tail->http2HeaderField->name,
                             header_list.tail->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(new_header_list->tail->http2HeaderField->value,
                             header_list.tail->http2HeaderField->value,
                             header_list.tail->http2HeaderField->valueLength));

    http2StreamDeallocate(stream);
    http2HeaderListDeallocate(new_header_list);
    http2BufferDeallocate(buffer);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
}
