/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "connection.h"
#include <nshttp2/connection.h>
#include <nshttp2/unity/unity.h>
#include <string.h>

void test_http2_connection_allocate(void) {
    Http2SettingsFramePayloadT received_settings;
    Http2SettingsFramePayloadT sent_settings;
    Http2ConnectionSettingsT settings;
    Http2ConnectionT *http2_connection;

    received_settings.enablePush = false;
    received_settings.headerTableSize = 100u;
    received_settings.initialWindowSize = UINT16_MAX;
    received_settings.maxConcurrentStreams = 128u;
    received_settings.maxFrameSize = UINT16_MAX;
    received_settings.maxHeaderListSize = 100u;

    sent_settings.enablePush = false;
    sent_settings.headerTableSize = 100u;
    sent_settings.initialWindowSize = UINT16_MAX;
    sent_settings.maxConcurrentStreams = 128u;
    sent_settings.maxFrameSize = UINT16_MAX;
    sent_settings.maxHeaderListSize = 100u;

    settings.closeConnectionFunction = NULL;
    settings.isClient = false;
    settings.receivedSettings = received_settings;
    settings.sentSettings = sent_settings;
    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;


    http2ConnectionAllocate(&http2_connection, &settings);

    TEST_ASSERT_EQUAL(settings.closeConnectionFunction,
                      http2_connection->closeConnectionFunction);
    TEST_ASSERT_EQUAL(settings.isClient,
                      http2_connection->isClient);

    TEST_ASSERT_NOT_NULL(http2_connection->streamIdToStreamMap);

    TEST_ASSERT_EQUAL((int32_t) received_settings.initialWindowSize,
                      http2_connection->remainingSendWindow);
    TEST_ASSERT_EQUAL((int32_t) sent_settings.initialWindowSize,
                      http2_connection->remainingReceiveWindow);

    TEST_ASSERT_EQUAL(1u, http2_connection->lastSentStreamId);
    TEST_ASSERT_EQUAL(0u, http2_connection->lastReceivedStreamId);

    TEST_ASSERT_NOT_NULL(http2_connection->inflater);
    TEST_ASSERT_NOT_NULL(http2_connection->deflater);

    http2ConnectionDeallocateAndClose(http2_connection);
}

void test_http2_connection_deallocate_and_close(void) {
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;
    settings.isClient = false;
    settings.closeConnectionFunction = NULL;

    http2ConnectionAllocate(&connection, &settings);

    http2ConnectionDeallocateAndClose(connection);
}

void test_http2_connection_process_frame(void) {
    /* Step 1 - Initialize the connection */
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;
    Http2WindowUpdatePayloadT window_update_payload;
    Http2FrameT frame;
    Http2PingFramePayloadT ping_frame_payload;
    uint8_t i;
    Http2HeaderFieldT first_field;
    Http2HeaderFieldT second_field;
    Http2HeaderListElementT second_element;
    Http2HeaderListElementT first_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT headers_frame_payload;
    Http2StreamT *stream;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;
    settings.isClient = false;
    settings.closeConnectionFunction = NULL;

    /* Step 2 - Test window update frame */
    window_update_payload.windowSizeIncrement = 50000u;

    frame.header.type = WINDOW_UPDATE_FRAME_TYPE;
    frame.header.length = HTTP2_WINDOW_UPDATE_PAYLOAD_LEN;
    frame.header.flags = 0u;
    frame.header.streamId = 0u;
    frame.payload.http2WindowUpdatePayload = &window_update_payload;

    http2ConnectionAllocate(&connection, &settings);

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS, http2ConnectionProcessFrame(connection, &frame, false));
    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE + 50000,
                      connection->remainingSendWindow);

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS, http2ConnectionProcessFrame(connection, &frame, true));
    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE + 50000,
                      connection->remainingReceiveWindow);

    for (i = 0u; i < 8; ++i) {
        ping_frame_payload.opaqueData[i] = i;
    }

    /* Step 3 - Test Ping frame */
    frame.header.length = HTTP2_PING_FRAME_PAYLOAD_SIZE;
    frame.header.type = PING_FRAME_TYPE;
    frame.payload.http2PingFramePayload = &ping_frame_payload;

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS, http2ConnectionProcessFrame(connection, &frame, false));

    frame.header.type = RST_STREAM_FRAME_TYPE;
    TEST_ASSERT_EQUAL(HTTP2_UNSUPPORTED_FRAME_TYPE,
                      http2ConnectionProcessFrame(connection, &frame, false));

    first_field.name = (uint8_t *) "user-agent";
    first_field.nameLength = strlen("user-agent");
    first_field.value = (uint8_t *) "test";
    first_field.valueLength = strlen("test");

    second_field.name = (uint8_t *) "user-agent";
    second_field.nameLength = strlen("user-agent");
    second_field.value = (uint8_t *) "kjskdjkadjksajdksajdowieuqowhroiqwhrtowqhotgohdsogodsofhosdhfosdf";
    second_field.valueLength = strlen("kjskdjkadjksajdksajdowieuqowhroiqwhrtowqhotgohdsogodsofhosdhfosdf");

    second_element.http2HeaderField = &second_field;
    second_element.next = NULL;

    first_element.http2HeaderField = &first_field;
    first_element.next = &second_element;

    header_list.head = &first_element;
    header_list.tail = &second_element;
    header_list.length = 2u;

    http2HeadersEncode(&header_list,
                       &buffer,
                       connection->deflater);

    headers_frame_payload.headerPayload = buffer->buffer;
    headers_frame_payload.priority.exclusive = true;
    headers_frame_payload.priority.dependency = 3u;
    headers_frame_payload.priority.weight = 250u;

    /* Step 4 - Test headers receive frame */
    frame.header.type = HEADERS_FRAME_TYPE;
    frame.header.length = buffer->length + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    frame.header.flags = END_HEADERS_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    frame.header.streamId = 5u;
    frame.payload.http2HeadersFramePayload = &headers_frame_payload;

    http2ConnectionProcessStreamFrame(connection, &frame, false);

    TEST_ASSERT_EQUAL(2u, kh_size(connection->streamIdToStreamMap->hashmap));

    kh_foreach_value(connection->streamIdToStreamMap->hashmap, stream, {
        if (stream->streamId == 3u) {
            Http2StreamT *child_stream = NULL;
            streamHashmapGet(stream->children, 5u, &child_stream);
            TEST_ASSERT_NOT_NULL(child_stream);
        } else if (stream->streamId == 5u) {
            Http2HeaderListT *received_headers;

            TEST_ASSERT_NOT_NULL(stream->parent);
            TEST_ASSERT_EQUAL(0u, stream->flags);
            TEST_ASSERT_EQUAL(3u, stream->parent->streamId);
            http2StreamDecodeHeaders(stream, &received_headers,
                                     connection->inflater);

            TEST_ASSERT_EQUAL(0, memcmp(received_headers->head->http2HeaderField->value,
                                        header_list.head->http2HeaderField->value,
                                        header_list.head->http2HeaderField->valueLength));

            http2HeaderListDeallocate(received_headers);
        } else {
            TEST_FAIL();
        }
    })

    http2BufferDeallocate(buffer);
    http2ConnectionDeallocateAndClose(connection);
}

void test_http2_connection_process_control_frame(void) {
    /* Test 1 - Initialize the connection */
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;
    Http2WindowUpdatePayloadT window_update_payload;
    Http2FrameT frame;
    Http2PingFramePayloadT ping_frame_payload;
    uint8_t i;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;
    settings.isClient = false;
    settings.closeConnectionFunction = NULL;

    /* Step 2 - Test window update frame */
    window_update_payload.windowSizeIncrement = 50000u;

    frame.header.type = WINDOW_UPDATE_FRAME_TYPE;
    frame.header.length = HTTP2_WINDOW_UPDATE_PAYLOAD_LEN;
    frame.header.flags = DEFAULT_FRAME_FLAG;
    frame.header.streamId = 0u;
    frame.payload.http2WindowUpdatePayload = &window_update_payload;

    http2ConnectionAllocate(&connection, &settings);

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS,
                      http2ConnectionProcessNonStreamFrame(connection, &frame, false));
    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE + 50000,
                      connection->remainingSendWindow);

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS,
                      http2ConnectionProcessNonStreamFrame(connection, &frame, true));
    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE + 50000,
                      connection->remainingReceiveWindow);

    for (i = 0u; i < 8; ++i) {
        ping_frame_payload.opaqueData[i] = i;
    }

    /* Test 3 - Test Ping frame */
    frame.header.length = HTTP2_PING_FRAME_PAYLOAD_SIZE;
    frame.header.type = PING_FRAME_TYPE;
    frame.payload.http2PingFramePayload = &ping_frame_payload;

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS,
                      http2ConnectionProcessNonStreamFrame(connection, &frame, false));

    /* Test 4 - RST Stream */
    frame.header.type = RST_STREAM_FRAME_TYPE;
    TEST_ASSERT_EQUAL(HTTP2_UNSUPPORTED_FRAME_TYPE,
                      http2ConnectionProcessNonStreamFrame(connection, &frame, false));

    /* Test 5 - Goaway frame */
    frame.header.type = PUSH_PROMISE_FRAME_TYPE;
    TEST_ASSERT_EQUAL(HTTP2_SUCCESS,
                      http2ConnectionProcessNonStreamFrame(connection, &frame, false));

    /* Test 6 */
    frame.header.type = GOAWAY_FRAME_TYPE;
    http2ConnectionProcessNonStreamFrame(connection, &frame, false);

    /* Cleanup */
    http2ConnectionDeallocateAndClose(connection);
}

void test_http2_connection_create_goaway(void) {
    /* Test 1 - Initialize the connection */
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;
    uint8_t buffer[HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN + HTTP2_FRAME_HEADER_SIZE];
    Http2GoawayFramePayloadT goaway_frame_payload;
    Http2FrameT goaway_frame;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;
    settings.isClient = false;
    settings.closeConnectionFunction = NULL;

    http2ConnectionAllocate(&connection, &settings);

    /* Test 2 - test goaway */
    http2ConnectionCreateGoaway(connection, PROTOCOL_ERROR_ERROR_CODE, buffer);

    goaway_frame.payload.http2GoawayFramePayload = &goaway_frame_payload;

    http2FrameFromBytes(&goaway_frame, false, buffer);

    TEST_ASSERT_EQUAL(GOAWAY_FRAME_TYPE, goaway_frame.header.type);
    TEST_ASSERT_EQUAL(0u, goaway_frame.header.streamId);
    TEST_ASSERT_EQUAL(0u, goaway_frame.header.flags);
    TEST_ASSERT_EQUAL(HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN, goaway_frame.header.length);
    TEST_ASSERT_EQUAL(1u, goaway_frame.payload.http2GoawayFramePayload->lastStreamId);
    TEST_ASSERT_EQUAL(PROTOCOL_ERROR_ERROR_CODE,
                      goaway_frame.payload.http2GoawayFramePayload->errorCode);

    /* Cleanup */
    http2ConnectionDeallocateAndClose(connection);
}

void test_http2_connection_process_data_frame(void) {
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    Http2FrameT frame;
    Http2StreamT *stream;
    uint8_t data_frame_payload[8];
    uint8_t i;
    Http2FrameT data_frame;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.isClient = false;
    settings.closeConnectionFunction = NULL;
    settings.maxIncomingDataInMemory = 5u;
    settings.temporaryFileTemplate = "/tmp/myTmpFile-XXXXXX";

    http2ConnectionAllocate(&connection, &settings);

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;


    http2HeadersEncode(&header_list,
                       &buffer,
                       connection->deflater);

    payload.headerPayload = buffer->buffer;

    frame.header.streamId = 3u;
    frame.header.type = HEADERS_FRAME_TYPE;
    frame.header.flags = END_HEADERS_FRAME_FLAG;
    frame.header.length = buffer->length;
    frame.payload.http2HeadersFramePayload = &payload;

    http2ConnectionProcessFrame(connection, &frame, true);

    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE, connection->remainingSendWindow);
    TEST_ASSERT_EQUAL(1u, kh_size(connection->streamIdToStreamMap->hashmap));
    streamHashmapGet(connection->streamIdToStreamMap, 3u, &stream);
    TEST_ASSERT_EQUAL(OPEN, stream->state);
    TEST_ASSERT_EQUAL(0u, stream->flags);

    for (i = 0u; i < 8u; ++i) {
        data_frame_payload[i] = i;
    }

    data_frame.header.streamId = 3u;
    data_frame.header.type = DATA_FRAME_TYPE;
    data_frame.header.length = 8u;
    data_frame.header.flags = DEFAULT_FRAME_FLAG | END_STREAM_FRAME_FLAG;
    data_frame.payload.http2DataFramePayload = data_frame_payload;

    http2ConnectionProcessFrame(connection, &data_frame, true);

    TEST_ASSERT_EQUAL(HALF_CLOSED_LOCAL, stream->state);
    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE - 8u,
                      connection->remainingSendWindow);
    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE - 8u,
                      stream->remainingSendWindow);

    /* Cleanup */
    http2BufferDeallocate(buffer);
    http2ConnectionDeallocateAndClose(connection);
}

void test_http2_connection_process_headers_frame(void) {
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    Http2FrameT frame;
    Http2StreamT *stream;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;
    settings.isClient = false;
    settings.closeConnectionFunction = NULL;

    http2ConnectionAllocate(&connection, &settings);

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.http2HeaderField = &host;
    host_element.next = &user_agent_element;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    http2HeadersEncode(&header_list,
                       &buffer,
                       connection->deflater);

    frame.header.streamId = 3u;
    frame.header.type = HEADERS_FRAME_TYPE;
    frame.header.flags = END_HEADERS_FRAME_FLAG;
    frame.header.length = buffer->length;

    http2ConnectionProcessFrame(connection, &frame, true);

    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE, connection->remainingSendWindow);
    TEST_ASSERT_EQUAL(1u, kh_size(connection->streamIdToStreamMap->hashmap));
    streamHashmapGet(connection->streamIdToStreamMap, 3u, &stream);
    TEST_ASSERT_EQUAL(OPEN, stream->state);
    TEST_ASSERT_EQUAL(0u, stream->flags);

    /* Cleanup */
    http2BufferDeallocate(buffer);
    http2ConnectionDeallocateAndClose(connection);
}

void test_http2_connection_process_continuation_frame(void) {
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    Http2FrameT frame;
    Http2StreamT *stream;
    Http2ContinuationPayloadT continuation_payload;
    Http2HeaderListT *received_headers;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;
    settings.isClient = false;
    settings.closeConnectionFunction = NULL;

    http2ConnectionAllocate(&connection, &settings);

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.http2HeaderField = &host;
    host_element.next = &user_agent_element;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    http2HeadersEncode(&header_list,
                       &buffer,
                       connection->deflater);

    payload.headerPayload = buffer->buffer;

    frame.header.streamId = 3u;
    frame.header.type = HEADERS_FRAME_TYPE;
    frame.header.flags = END_HEADERS_FRAME_FLAG;
    frame.header.length = buffer->length;

    payload.headerPayload = buffer->buffer;

    frame.header.streamId = 3u;
    frame.header.type = HEADERS_FRAME_TYPE;
    frame.header.flags = DEFAULT_FRAME_FLAG;
    frame.header.length = buffer->length - 2u;
    frame.payload.http2HeadersFramePayload = &payload;

    http2ConnectionProcessFrame(connection, &frame, false);

    streamHashmapGet(connection->streamIdToStreamMap, 3u, &stream);

    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE, connection->remainingSendWindow);
    TEST_ASSERT_EQUAL(1u, kh_size(connection->streamIdToStreamMap->hashmap));
    TEST_ASSERT_EQUAL(buffer->length - 2u, stream->incomingHeadersBuffer->length);
    TEST_ASSERT_EQUAL(OPEN, stream->state);
    TEST_ASSERT_EQUAL(HTTP2_STREAM_CONTINUATION_EXPECTED, stream->flags);

    frame.header.type = CONTINUATION_FRAME_TYPE;
    frame.header.flags = END_HEADERS_FRAME_FLAG;
    frame.header.length = 2u;

    continuation_payload.headerBlockFragment = buffer->buffer + (buffer->length - 2u);

    frame.payload.http2ContinuationPayload = &continuation_payload;

    http2ConnectionProcessFrame(connection, &frame, false);

    TEST_ASSERT_EQUAL(buffer->length, stream->incomingHeadersBuffer->length);

    http2StreamDecodeHeaders(stream, &received_headers, connection->inflater);

    TEST_ASSERT_EQUAL(0,
                      memcmp(received_headers->head->http2HeaderField->name,
                             header_list.head->http2HeaderField->name,
                             header_list.head->http2HeaderField->nameLength));
    TEST_ASSERT_EQUAL(0,
                      memcmp(received_headers->head->http2HeaderField->value,
                             header_list.head->http2HeaderField->value,
                             header_list.head->http2HeaderField->valueLength));

    TEST_ASSERT_EQUAL(header_list.length, received_headers->length);

    TEST_ASSERT_EQUAL(0,
                      memcmp(received_headers->tail->http2HeaderField->name,
                             header_list.tail->http2HeaderField->name,
                             header_list.tail->http2HeaderField->nameLength));
    TEST_ASSERT_EQUAL(0,
                      memcmp(received_headers->tail->http2HeaderField->value,
                             header_list.tail->http2HeaderField->value,
                             header_list.tail->http2HeaderField->valueLength));

    /* Cleanup */
    http2HeaderListDeallocate(received_headers);
    http2BufferDeallocate(buffer);
    http2ConnectionDeallocateAndClose(connection);
}

void test_http2_connection_process_priority(void) {
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;
    Http2PriorityFramePayloadT priority_frame_payload;
    Http2FrameT priority_frame;
    Http2StreamT *stream;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;
    settings.isClient = false;
    settings.closeConnectionFunction = NULL;

    http2ConnectionAllocate(&connection, &settings);

    priority_frame_payload.dependency = 5u;
    priority_frame_payload.weight = 255u;
    priority_frame_payload.exclusive = true;

    priority_frame.header.length = HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    priority_frame.header.flags = DEFAULT_FRAME_FLAG;
    priority_frame.header.type = PRIORITY_FRAME_TYPE;
    priority_frame.header.streamId = 3u;
    priority_frame.payload.http2PriorityFramePayload = &priority_frame_payload;

    http2ConnectionProcessFrame(connection, &priority_frame, false);

    streamHashmapGet(connection->streamIdToStreamMap, 3u, &stream);

    TEST_ASSERT_NOT_NULL(stream);
    TEST_ASSERT_NOT_NULL(stream->parent);
    TEST_ASSERT_EQUAL(5u, stream->parent->streamId);

    stream = NULL;
    streamHashmapGet(connection->streamIdToStreamMap, 5u, &stream);

    TEST_ASSERT_NOT_NULL(stream);
    TEST_ASSERT_NOT_NULL(stream->children);

    /* Cleanup */
    http2ConnectionDeallocateAndClose(connection);
}

void test_http2_connection_create_stream(void) {
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;
    Http2StreamT *stream = NULL;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;
    settings.isClient = false;
    settings.closeConnectionFunction = NULL;

    http2ConnectionAllocate(&connection, &settings);

    http2ConnectionCreateStream(connection, 3u, 8u, &stream);

    stream = NULL;
    streamHashmapGet(connection->streamIdToStreamMap, 3u, &stream);

    TEST_ASSERT_EQUAL(3u, stream->streamId);
    TEST_ASSERT_EQUAL(8u, stream->weight);
    TEST_ASSERT_EQUAL(0u, stream->flags);
    TEST_ASSERT_EQUAL(connection->receivedSettings.initialWindowSize, stream->remainingSendWindow);

    http2ConnectionDeallocateAndClose(connection);
}

void test_http2_connection_process_headers_frame_with_priority(void) {
    Http2ConnectionT *connection;
    Http2ConnectionSettingsT settings;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    Http2FrameT frame;
    Http2StreamT *stream;

    settings.sentSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.sentSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.sentSettings.enablePush = false;
    settings.sentSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.sentSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.sentSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.receivedSettings.maxHeaderListSize = DEFAULT_SETTING_MAX_HEADER_LIST_SIZE;
    settings.receivedSettings.headerTableSize = DEFAULT_SETTING_HEADER_TABLE_SIZE;
    settings.receivedSettings.enablePush = false;
    settings.receivedSettings.maxConcurrentStreams = DEFAULT_SETTING_MAX_CONCURRENT_STREAMS;
    settings.receivedSettings.maxFrameSize = DEFAULT_SETTING_MAX_FRAME_SIZE;
    settings.receivedSettings.initialWindowSize = DEFAULT_SETTING_INITIAL_WINDOW_SIZE;

    settings.maxIncomingDataInMemory = 10u * 1024u * 1024u;
    settings.temporaryFileTemplate = NULL;
    settings.isClient = false;
    settings.closeConnectionFunction = NULL;

    http2ConnectionAllocate(&connection, &settings);

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    http2HeadersEncode(&header_list,
                       &buffer,
                       connection->deflater);

    payload.priority.exclusive = true;
    payload.priority.dependency = 1u;
    payload.priority.weight = 230u;
    payload.headerPayload = buffer->buffer;

    frame.header.streamId = 3u;
    frame.header.type = HEADERS_FRAME_TYPE;
    frame.header.flags = END_HEADERS_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    frame.header.length = buffer->length + HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE;
    frame.payload.http2HeadersFramePayload = &payload;

    http2ConnectionProcessFrame(connection, &frame, true);

    TEST_ASSERT_EQUAL(DEFAULT_SETTING_INITIAL_WINDOW_SIZE, connection->remainingSendWindow);
    TEST_ASSERT_EQUAL(1u, kh_size(connection->streamIdToStreamMap->hashmap));
    streamHashmapGet(connection->streamIdToStreamMap, 3u, &stream);
    TEST_ASSERT_EQUAL(OPEN, stream->state);
    TEST_ASSERT_EQUAL(0u, stream->flags);

    /* Cleanup */
    http2BufferDeallocate(buffer);
    http2ConnectionDeallocateAndClose(connection);
}
