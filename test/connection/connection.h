/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NSHTTP2_CONNECTION_H
#define NSHTTP2_CONNECTION_H

void test_http2_connection_allocate(void);

void test_http2_connection_deallocate_and_close(void);

void test_http2_connection_process_frame(void);

void test_http2_connection_process_control_frame(void);

void test_http2_connection_process_non_control_frame(void);

void test_http2_connection_create_goaway(void);

void test_http2_connection_process_data_frame(void);

void test_http2_connection_process_priority(void);

void test_http2_connection_process_headers_frame(void);

void test_http2_connection_process_headers_frame_with_priority(void);

void test_http2_connection_process_continuation_frame(void);

void test_http2_connection_create_stream(void);

void test_http2_connection_get_or_create_stream(void);

void test_http2_connection_remove_and_deallocate_stream(void);

void test_http2_connection_process_window_update_frame_on_stream(void);

void test_http2_connection_process_rst_stream_frame(void);

void test_http2_connection_stream_id_state(void);

#endif /* NSHTTP2_CONNECTION_H */
