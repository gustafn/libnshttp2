/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/unity/unity.h>
#include <nshttp2/utility/memory.h>
#include <test/frame/frame.h>
#include <test/headers/headers.h>
#include <test/memory/memory.h>
#include <test/stream/stream.h>
#include <test/string_utils/string_utils.h>
#include <test/stream_hashmap/stream_hashmap.h>
#include <test/buffer/buffer.h>
#include <test/connection/connection.h>
#include <test/frame_payload/frame_payload.h>
#include <test/frame_list/frame_list.h>

void setUp(void) {}

void tearDown(void) {}

int main(void) {
    Http2MemoryAllocatorsT memoryAllocators;
    memoryAllocators.malloc = malloc;
    memoryAllocators.calloc = calloc;
    memoryAllocators.free = free;
    memoryAllocators.realloc = realloc;

    http2SetupMemoryAllocators(&memoryAllocators);

    UNITY_BEGIN();

    RUN_TEST(test_http2_malloc);
    RUN_TEST(test_http2_calloc);
    RUN_TEST(test_http2_free);
    RUN_TEST(test_http2_realloc);

    RUN_TEST(test_http2_buffer_allocate);
    RUN_TEST(test_http2_buffer_extend);
    RUN_TEST(test_http2_buffer_deallocate);
    RUN_TEST(test_http2_buffer_append);
    RUN_TEST(test_http2BufferMoveByOffset);

    RUN_TEST(test_stream_hashmap_allocate);
    RUN_TEST(test_stream_hashmap_insert);
    RUN_TEST(test_stream_hashmap_insert_with_stream_id);

    RUN_TEST(test_http2_header_field_allocate);
    RUN_TEST(test_http2_header_field_deallocate);
    RUN_TEST(test_http2_header_field_to_nv);
    RUN_TEST(test_http2_headers_to_nv_array);
    RUN_TEST(test_http2_header_allocate);
    RUN_TEST(test_http2_header_list_deallocate);
    RUN_TEST(test_http2_header_from_nghttp2_nv);
    RUN_TEST(test_http2_headers_decode);
    RUN_TEST(test_http2_headers_encode);
    RUN_TEST(test_http2_headers_decode_differential_compression);

    RUN_TEST(test_http2_priority_frame_payload_to_bytes);
    RUN_TEST(test_http2_priority_frame_payload_from_bytes);
    RUN_TEST(test_http2_data_frame_payload_to_bytes);
    RUN_TEST(test_http2_data_frame_payload_from_bytes);
    RUN_TEST(test_http2_headers_frame_payload_to_bytes);
    RUN_TEST(test_http2_headers_frame_payload_from_bytes);
    RUN_TEST(test_http2_rst_stream_frame_payload_to_bytes);
    RUN_TEST(test_http2_rst_stream_frame_payload_from_bytes);
    RUN_TEST(test_http2_settings_frame_payload_to_bytes);
    RUN_TEST(test_http2_settings_frame_payload_from_bytes);
    RUN_TEST(test_http2_push_promise_frame_payload_to_bytes);
    RUN_TEST(test_http2_push_promise_frame_payload_from_bytes);
    RUN_TEST(test_http2_ping_frame_payload_to_bytes);
    RUN_TEST(test_http2_ping_frame_payload_from_bytes);
    RUN_TEST(test_http2_goaway_frame_payload_to_bytes);
    RUN_TEST(test_http2_goaway_frame_payload_from_bytes);
    RUN_TEST(test_http2_goaway_frame_payload_from_bytes_with_debug_data);
    RUN_TEST(test_http2_window_update_payload_to_bytes);
    RUN_TEST(test_http2_window_update_payload_from_bytes);
    RUN_TEST(test_http2_continuation_payload_to_bytes);
    RUN_TEST(test_http2_continuation_payload_from_bytes);
    RUN_TEST(test_http2_priority_frame_payload_allocate);
    RUN_TEST(test_http2_priority_frame_payload_deallocate);
    RUN_TEST(test_http2_headers_frame_payload_allocate);
    RUN_TEST(test_http2_headers_frame_payload_deallocate);
    RUN_TEST(test_http2_rst_stream_frame_payload_allocate);
    RUN_TEST(test_http2_rst_stream_frame_payload_deallocate);
    RUN_TEST(test_http2_settings_frame_payload_allocate);
    RUN_TEST(test_http2_settings_frame_payload_deallocate);
    RUN_TEST(test_http2_push_promise_frame_payload_allocate);
    RUN_TEST(test_http2_push_promise_frame_payload_deallocate);
    RUN_TEST(test_http2_ping_frame_payload_allocate);
    RUN_TEST(test_http2_ping_frame_payload_deallocate);
    RUN_TEST(test_http2_goaway_frame_payload_allocate);
    RUN_TEST(test_http2_frame_goaway_frame_payload_allocate_debug_data);
    RUN_TEST(test_http2_goaway_frame_payload_deallocate);
    RUN_TEST(test_http2_window_update_payload_allocate);
    RUN_TEST(test_http2_window_update_payload_deallocate);
    RUN_TEST(test_http2_continuation_payload_allocate);
    RUN_TEST(test_http2_continuation_payload_deallocate);
    RUN_TEST(test_http2_buffer_remove_padding);
    RUN_TEST(test_http2_is_non_zero_header_buffer);
    RUN_TEST(test_http2_frame_header_to_bytes);
    RUN_TEST(test_http2_frame_header_from_bytes);

    RUN_TEST(test_http2_frame_from_bytes_header_set_data_frame);
    RUN_TEST(test_http2_frame_from_bytes_header_set_headers_frame);
    RUN_TEST(test_http2_frame_from_bytes_header_set_priority_frame);
    RUN_TEST(test_http2_frame_from_bytes_header_set_rst_stream_frame);
    RUN_TEST(test_http2_frame_from_bytes_header_set_settings_frame);
    RUN_TEST(test_http2_frame_from_bytes_header_set_push_promise_frame);
    RUN_TEST(test_http2_frame_from_bytes_header_set_ping_frame);
    RUN_TEST(test_http2_frame_from_bytes_header_set_goaway_frame);
    RUN_TEST(test_http2_frame_from_bytes_header_set_window_update_frame);
    RUN_TEST(test_http2_frame_from_bytes_header_set_continuation_frame);

    RUN_TEST(test_http2_frame_from_bytes_data_frame);
    RUN_TEST(test_http2_frame_from_bytes_headers_frame);
    RUN_TEST(test_http2_frame_from_bytes_priority_frame);
    RUN_TEST(test_http2_frame_from_bytes_rst_stream_frame);
    RUN_TEST(test_http2_frame_from_bytes_settings_frame);
    RUN_TEST(test_http2_frame_from_bytes_push_promise_frame);
    RUN_TEST(test_http2_frame_from_bytes_ping_frame);
    RUN_TEST(test_http2_frame_from_bytes_goaway_frame);
    RUN_TEST(test_http2_frame_from_bytes_window_update_frame);
    RUN_TEST(test_http2_frame_from_bytes_continuation_frame);

    RUN_TEST(test_http2_frame_data_frame_payload_allocate);
    RUN_TEST(test_http2_frame_headers_frame_payload_allocate);
    RUN_TEST(test_http2_frame_priority_frame_payload_allocate);
    RUN_TEST(test_http2_frame_rst_stream_frame_payload_allocate);
    RUN_TEST(test_http2_frame_settings_frame_payload_allocate);
    RUN_TEST(test_http2_frame_push_promise_frame_payload_allocate);
    RUN_TEST(test_http2_frame_ping_frame_payload_allocate);
    RUN_TEST(test_http2_frame_goaway_frame_payload_allocate);
    RUN_TEST(test_http2_frame_window_update_frame_payload_allocate);
    RUN_TEST(test_http2_frame_continuation_frame_payload_allocate);
    RUN_TEST(test_http2_frame_to_bytes_frame_with_allocation);
    RUN_TEST(test_http2_frame_is_non_control_frame);
    RUN_TEST(test_http2_frame_is_control_frame);
    RUN_TEST(test_http2_frame_has_priority);
    RUN_TEST(test_http2_frame_is_last_header);
    RUN_TEST(test_http2_is_valid_connection_preface);
    RUN_TEST(test_http2_frame_type_name);

    RUN_TEST(test_http2_frame_list_allocate);
    RUN_TEST(test_http2_frame_list_append);
    RUN_TEST(test_http2_frame_list_allocate_empty);
    RUN_TEST(test_http2_frame_list_join);
    RUN_TEST(test_http2_frame_list_to_bytes);
    RUN_TEST(test_http2_frame_list_from_bytes);
    RUN_TEST(test_http2_frame_list_element_remove);

    RUN_TEST(test_http2_stream_allocate);
    RUN_TEST(test_http2_stream_deallocate);
    RUN_TEST(test_http2_stream_add_child);
    RUN_TEST(test_http2_stream_remove_child);
    RUN_TEST(test_http2_stream_add_sibling);
    RUN_TEST(test_http2_stream_remove_sibling);
    RUN_TEST(test_http2_stream_set_state);
    RUN_TEST(test_http2_stream_frame_get_error);
    RUN_TEST(test_http2_stream_add_header_block);
    RUN_TEST(test_http2_stream_decode_headers);

    RUN_TEST(test_http2_connection_allocate);
    RUN_TEST(test_http2_connection_deallocate_and_close);
    RUN_TEST(test_http2_connection_process_frame);
    RUN_TEST(test_http2_connection_process_control_frame);
    RUN_TEST(test_http2_connection_create_goaway);
    RUN_TEST(test_http2_connection_process_data_frame);
    RUN_TEST(test_http2_connection_process_headers_frame);
    RUN_TEST(test_http2_connection_process_headers_frame_with_priority);
    RUN_TEST(test_http2_connection_process_continuation_frame);
    RUN_TEST(test_http2_connection_process_priority);
    RUN_TEST(test_http2_connection_create_stream);

    RUN_TEST(test_string_start_with_substring);
    RUN_TEST(test_stringDuplicate);

    return UNITY_END();
}
