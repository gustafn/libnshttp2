/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "buffer.h"
#include <nshttp2/unity/unity.h>
#include <nshttp2/buffer/buffer.h>
#include <string.h>

void test_http2_buffer_allocate(void) {
    Http2BufferT *buffer;

    switch (http2BufferAllocate(&buffer, 0u)) {
        case HTTP2_SUCCESS:
            TEST_ASSERT_NOT_NULL(buffer);
            TEST_ASSERT_NULL(buffer->buffer);
            TEST_ASSERT_EQUAL_size_t(0u, buffer->length);
            http2BufferDeallocate(buffer);
            break;
        case HTTP2_NO_MEMORY:
            TEST_ASSERT_NULL(buffer);
            break;
        default:
            TEST_FAIL();
    }

    switch (http2BufferAllocate(&buffer, 1u)) {
        case HTTP2_SUCCESS:
            TEST_ASSERT_NOT_NULL(buffer);
            TEST_ASSERT_NOT_NULL(buffer->buffer);
            TEST_ASSERT_EQUAL_size_t(1u, buffer->length);
            http2BufferDeallocate(buffer);
            break;
        case HTTP2_NO_MEMORY:
            TEST_ASSERT_NULL(buffer);
            break;
        default:
            TEST_FAIL();
    }

    switch (http2BufferAllocate(&buffer, 128u)) {
        case HTTP2_SUCCESS:
            TEST_ASSERT_NOT_NULL(buffer);
            TEST_ASSERT_NOT_NULL(buffer->buffer);
            TEST_ASSERT_EQUAL_size_t(128u, buffer->length);
            http2BufferDeallocate(buffer);
            break;
        case HTTP2_NO_MEMORY:
            TEST_ASSERT_NULL(buffer);
            break;
        default:
            TEST_FAIL();
    }
}

void test_http2_buffer_deallocate(void) {
    Http2BufferT *buffer;

    switch (http2BufferAllocate(&buffer, 0u)) {
        case HTTP2_SUCCESS:
            TEST_ASSERT_NOT_NULL(buffer);
            TEST_ASSERT_NULL(buffer->buffer);
            TEST_ASSERT_EQUAL_size_t(0u, buffer->length);
            http2BufferDeallocate(buffer);
            break;
        case HTTP2_NO_MEMORY:
            TEST_ASSERT_NULL(buffer);
            break;
        default:
            TEST_FAIL();
    }

    switch (http2BufferAllocate(&buffer, 128u)) {
        case HTTP2_SUCCESS:
            TEST_ASSERT_NOT_NULL(buffer);
            TEST_ASSERT_NOT_NULL(buffer->buffer);
            TEST_ASSERT_EQUAL_size_t(128u, buffer->length);
            http2BufferDeallocate(buffer);
            break;
        case HTTP2_NO_MEMORY:
            TEST_ASSERT_NULL(buffer);
            break;
        default:
            TEST_FAIL();
    }
}

void test_http2_buffer_extend(void) {
    Http2BufferT *buffer;
    Http2ReturnCodeT return_code;

    http2BufferAllocate(&buffer, 0u);

    return_code = http2BufferExtend(buffer, 0u);

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS, return_code);
    TEST_ASSERT_NULL(buffer->buffer);
    TEST_ASSERT_EQUAL_size_t(0u, buffer->length);

    return_code = http2BufferExtend(buffer, 128u);

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS, return_code);
    TEST_ASSERT_NOT_NULL(buffer->buffer);
    TEST_ASSERT_EQUAL_size_t(128u, buffer->length);

    http2BufferDeallocate(buffer);
}

void test_http2_buffer_append(void) {
    Http2BufferT *buffer;
    Http2ReturnCodeT return_code;
    uint8_t buffer_to_append[128];
    uint8_t i;

    http2BufferAllocate(&buffer, 0u);

    return_code = http2BufferExtend(buffer, 0u);

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS, return_code);
    TEST_ASSERT_NULL(buffer->buffer);
    TEST_ASSERT_EQUAL_size_t(0u, buffer->length);

    for (i = 0u; i < 128; ++i) {
        buffer_to_append[i] = 0u;
    }

    http2BufferAppend(buffer, buffer_to_append, 128u);

    TEST_ASSERT_NOT_NULL(buffer->buffer);
    TEST_ASSERT_EQUAL_size_t(128u, buffer->length);
    TEST_ASSERT_EQUAL_INT(0, memcmp(buffer->buffer, buffer_to_append, 128u));

    http2BufferAppend(buffer, buffer_to_append, 128u);
    TEST_ASSERT_NOT_NULL(buffer->buffer);
    TEST_ASSERT_EQUAL_size_t(256u, buffer->length);
    TEST_ASSERT_EQUAL_INT(0, memcmp(buffer->buffer, buffer_to_append, 128u));
    TEST_ASSERT_EQUAL_INT(0, memcmp(buffer->buffer + 128, buffer_to_append, 128u));

    http2BufferDeallocate(buffer);
}

void test_http2BufferMoveByOffset(void) {
    char *testString = "offsetTest";
    Http2BufferT *buffer;

    TEST_ASSERT_EQUAL(HTTP2_SUCCESS, http2BufferAllocate(&buffer, 10));
    TEST_ASSERT_NOT_NULL(buffer->buffer);
    TEST_ASSERT_EQUAL_size_t(buffer->length, 10);

    memcpy(buffer->buffer, testString, 10);

    http2BufferMoveByOffset(buffer, 6);

    TEST_ASSERT_NOT_NULL(buffer->buffer);
    TEST_ASSERT_EQUAL_size_t(buffer->length, 4);
    TEST_ASSERT_EQUAL_CHAR_ARRAY("Test", (char *) buffer->buffer, 4);

    http2BufferDeallocate(buffer);
}
