/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "stream_hashmap.h"
#include <nshttp2/stream_hashmap.h>
#include <nshttp2/stream.h>
#include <nshttp2/unity/unity.h>

void test_stream_hashmap_allocate(void) {
    StreamHashmapT *hashmap = NULL;
    streamHashmapAllocate(&hashmap);

    TEST_ASSERT_EQUAL(0u, kh_size(hashmap->hashmap));
    TEST_ASSERT_NOT_NULL(hashmap);

    streamHashmapDeallocate(hashmap);
}

void test_stream_hashmap_insert(void) {
    StreamHashmapT *hashmap;
    size_t i;
    Http2StreamT **streams;

    streamHashmapAllocate(&hashmap);

    streams = http2Malloc(sizeof(Http2StreamT *) * 128);

    for (i = 0u; i < 128u; ++i) {
        http2StreamAllocate(&streams[i],
                            i,
                            200u,
                            50000u,
                            53000u,
                            10u * 1024u * 1024u);
    }

    for (i = 0; i < 128u; ++i) {
        streamHashmapInsert(hashmap, streams[i]);
    }

    TEST_ASSERT_EQUAL(128u, kh_size(hashmap->hashmap));

    for (i = 0u; i < 128u; ++i) {
        http2StreamDeallocate(streams[i]);
    }

    streamHashmapDeallocate(hashmap);
    http2Free(streams);
}

void test_stream_hashmap_insert_with_stream_id(void) {
    StreamHashmapT *hashmap;
    Http2StreamT stream;
    Http2StreamT *new_stream;

    streamHashmapAllocate(&hashmap);

    stream.streamId = 4u;

    streamHashmapInsertWithStreamId(hashmap, (uint32_t) stream.streamId, &stream);

    streamHashmapGet(hashmap, stream.streamId, &new_stream);

    TEST_ASSERT_EQUAL(&stream, new_stream);

    streamHashmapDeallocate(hashmap);
}
