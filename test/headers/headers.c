/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "headers.h"
#include <nshttp2/headers.h>
#include <nshttp2/unity/unity.h>
#include <nshttp2/utility/nghttp2.h>

void test_http2_header_field_allocate(void) {
    Http2HeaderFieldT *field;
    http2HeaderFieldAllocate(&field,
                             strlen("user-agent"),
                             strlen("test"));

    TEST_ASSERT_NOT_NULL(field->value);
    TEST_ASSERT_NOT_NULL(field->name);
    TEST_ASSERT_EQUAL(strlen("user-agent"), field->nameLength);
    TEST_ASSERT_EQUAL(strlen("test"), field->valueLength);

    http2HeaderFieldDeallocate(field);
}

void test_http2_header_field_deallocate(void) {
    Http2HeaderFieldT *field;
    http2HeaderFieldAllocate(&field,
                             strlen("user-agent"),
                             strlen("test"));

    TEST_ASSERT_NOT_NULL(field->value);
    TEST_ASSERT_NOT_NULL(field->name);
    TEST_ASSERT_EQUAL(strlen("user-agent"), field->nameLength);
    TEST_ASSERT_EQUAL(strlen("test"), field->valueLength);

    http2HeaderFieldDeallocate(field);
}

void test_http2_header_field_to_nv(void) {
    Http2HeaderFieldT field;
    nghttp2_nv nv;

    field.name = (uint8_t *) "user-agent";
    field.nameLength = strlen("user-agent");
    field.value = (uint8_t *) "test";
    field.valueLength = strlen("test");

    http2HeaderFieldToNv(&field, &nv);

    TEST_ASSERT_EQUAL(0, memcmp(field.name, nv.name, field.nameLength));
    TEST_ASSERT_EQUAL(0, memcmp(field.value, nv.value, field.valueLength));

    http2Free(nv.value);
    http2Free(nv.name);
}

void test_http2_headers_to_nv_array(void) {
    Http2HeaderFieldT first_field;
    Http2HeaderFieldT second_field;
    Http2HeaderListElementT second_element;
    Http2HeaderListElementT first_element;
    Http2HeaderListT header_list;
    nghttp2_nv *nv;

    first_field.name = (uint8_t *) "user-agent";
    first_field.nameLength = strlen("user-agent");
    first_field.value = (uint8_t *) "test";
    first_field.valueLength = strlen("test");

    second_field.name = (uint8_t *) "user-agent";
    second_field.nameLength = strlen("user-agent");
    second_field.value = (uint8_t *) "test";
    second_field.valueLength = strlen("test");

    second_element.http2HeaderField = &second_field;
    second_element.next = NULL;

    first_element.http2HeaderField = &first_field;
    first_element.next = &second_element;

    header_list.head = &first_element;
    header_list.tail = &second_element;
    header_list.length = 2u;

    http2HeadersToNvArray(&header_list, &nv);

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.tail->http2HeaderField->name,
                             nv[1].name,
                             header_list.tail->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.tail->http2HeaderField->value,
                             nv[1].value,
                             header_list.tail->http2HeaderField->valueLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.tail->http2HeaderField->name,
                             nv[0].name,
                             header_list.tail->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.head->http2HeaderField->value,
                             nv[0].value,
                             header_list.head->http2HeaderField->valueLength));

    http2NvArrayDeallocate(nv, header_list.length);
}

void test_http2_header_allocate(void) {
    Http2HeaderListElementT *element;
    http2HeaderAllocate(&element, 100u, 100u);

    TEST_ASSERT_EQUAL(100u, element->http2HeaderField->nameLength);
    TEST_ASSERT_EQUAL(100u, element->http2HeaderField->valueLength);

    http2HeaderFieldDeallocate(element->http2HeaderField);
    http2Free(element);
}

void test_http2_header_list_deallocate(void) {
    Http2HeaderListElementT *first_element;
    Http2HeaderListElementT *second_element;
    Http2HeaderListT *header_list;

    http2HeaderAllocate(&first_element,
                        strlen("user-agent"),
                        strlen("test"));

    http2HeaderAllocate(&second_element,
                        strlen("user-agent"),
                        strlen("john"));

    memcpy(first_element->http2HeaderField->name, "user-agent", strlen("user-agent"));
    memcpy(first_element->http2HeaderField->value, "test", strlen("test"));
    memcpy(second_element->http2HeaderField->name, "user-agent", strlen("user-agent"));
    memcpy(second_element->http2HeaderField->value, "john", strlen("john"));

    first_element->next = second_element;

    header_list = http2Malloc(sizeof(Http2HeaderListT));

    header_list->head = first_element;
    header_list->tail = second_element;
    header_list->length = 2u;

    http2HeaderListDeallocate(header_list);
}

void test_http2_header_from_nghttp2_nv(void) {
    nghttp2_nv nv;
    Http2HeaderFieldT field;

    nv.name = (uint8_t *) "user-agent";
    nv.namelen = strlen("user-agent");
    nv.value = (uint8_t *) "test";
    nv.valuelen = strlen("test");
    nv.flags = 0u;

    field.name = http2Malloc(nv.namelen);
    field.nameLength = strlen("user-agent");
    field.value = http2Malloc(nv.valuelen);
    field.valueLength = strlen("test");

    http2HeaderFromNghttp2Nv(&nv, &field);

    TEST_ASSERT_EQUAL(0, memcmp(field.name, nv.name, field.nameLength));
    TEST_ASSERT_EQUAL(0, memcmp(field.value, nv.value, field.valueLength));

    http2Free(field.name);
    http2Free(field.value);
}

void test_http2_headers_decode(void) {
    Http2HeaderFieldT first_field;
    Http2HeaderFieldT second_field;
    Http2HeaderListElementT second_element;
    Http2HeaderListElementT first_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2HeaderListT *second_list;

    first_field.name = (uint8_t *) "user-agent";
    first_field.nameLength = strlen("user-agent");
    first_field.value = (uint8_t *) "test";
    first_field.valueLength = strlen("test");

    second_field.name = (uint8_t *) "user-agent";
    second_field.nameLength = strlen("user-agent");
    second_field.value = (uint8_t *) "john";
    second_field.valueLength = strlen("john");

    second_element.http2HeaderField = &second_field;
    second_element.next = NULL;

    first_element.http2HeaderField = &first_field;
    first_element.next = &second_element;

    header_list.head = &first_element;
    header_list.tail = &second_element;
    header_list.length = 2u;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    TEST_ASSERT_NOT_NULL(nghttp2HdDeflater);

    ngHttp2GetInflater(&nghttp2HdInflater);

    TEST_ASSERT_NOT_NULL(nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    http2HeadersDecode(buffer->buffer, (uint32_t) buffer->length, &second_list, nghttp2HdInflater);

    TEST_ASSERT_EQUAL(header_list.head->http2HeaderField->valueLength,
                      second_list->head->http2HeaderField->valueLength);
    TEST_ASSERT_EQUAL(header_list.head->http2HeaderField->nameLength,
                      second_list->head->http2HeaderField->nameLength);

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.head->http2HeaderField->name,
                             second_list->head->http2HeaderField->name,
                             header_list.head->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.tail->http2HeaderField->name,
                             second_list->tail->http2HeaderField->name,
                             header_list.tail->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.head->http2HeaderField->value,
                             second_list->head->http2HeaderField->value,
                             header_list.head->http2HeaderField->valueLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.tail->http2HeaderField->value,
                             second_list->tail->http2HeaderField->value,
                             header_list.tail->http2HeaderField->valueLength));

    http2BufferDeallocate(buffer);
    http2HeaderListDeallocate(second_list);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
}

void test_http2_headers_encode(void) {
    Http2HeaderFieldT first_field;
    Http2HeaderFieldT second_field;
    Http2HeaderListElementT second_element;
    Http2HeaderListElementT first_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2HeaderListT *second_list;

    first_field.name = (uint8_t *) "user-agent";
    first_field.nameLength = strlen("user-agent");
    first_field.value = (uint8_t *) "test";
    first_field.valueLength = strlen("test");

    second_field.name = (uint8_t *) "user-agent";
    second_field.nameLength = strlen("user-agent");
    second_field.value = (uint8_t *) "john";
    second_field.valueLength = strlen("john");

    second_element.http2HeaderField = &second_field;
    second_element.next = NULL;

    first_element.http2HeaderField = &first_field;
    first_element.next = &second_element;

    header_list.head = &first_element;
    header_list.tail = &second_element;
    header_list.length = 2u;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    TEST_ASSERT_NOT_NULL(nghttp2HdDeflater);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    http2HeadersDecode(buffer->buffer, (uint32_t) buffer->length, &second_list, nghttp2HdInflater);

    TEST_ASSERT_EQUAL(header_list.head->http2HeaderField->valueLength,
                      second_list->head->http2HeaderField->valueLength);
    TEST_ASSERT_EQUAL(header_list.head->http2HeaderField->nameLength,
                      second_list->head->http2HeaderField->nameLength);

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.head->http2HeaderField->name,
                             second_list->head->http2HeaderField->name,
                             header_list.head->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.tail->http2HeaderField->name,
                             second_list->tail->http2HeaderField->name,
                             header_list.tail->http2HeaderField->nameLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.head->http2HeaderField->value,
                             second_list->head->http2HeaderField->value,
                             header_list.head->http2HeaderField->valueLength));

    TEST_ASSERT_EQUAL(0,
                      memcmp(header_list.tail->http2HeaderField->value,
                             second_list->tail->http2HeaderField->value,
                             header_list.tail->http2HeaderField->valueLength));

    http2BufferDeallocate(buffer);
    http2HeaderListDeallocate(second_list);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
}

void test_http2_headers_decode_differential_compression(void) {
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    Http2HeaderListT *headerList;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer = NULL;
    Http2HeaderListT *newHeaderList = NULL;

    headerList = http2Calloc(sizeof(Http2HeaderListT));
    http2HeaderListAppend(headerList,
                          "user-agent",
                          strlen("user-agent"),
                          "chrome",
                          strlen("chrome"));

    http2HeaderListAppend(headerList,
                          "cookie",
                          strlen("cookie"),
                          "whatever",
                          strlen("whatever"));


    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    TEST_ASSERT_NOT_NULL(nghttp2HdDeflater);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(headerList, &buffer, nghttp2HdDeflater);

    http2HeadersDecode(buffer->buffer, (uint32_t) buffer->length, &newHeaderList, nghttp2HdInflater);

    http2HeaderListDeallocate(newHeaderList);
    http2BufferDeallocate(buffer);
    buffer = NULL;

    http2HeadersEncode(headerList, &buffer, nghttp2HdDeflater);
    newHeaderList = NULL;
    http2HeadersDecode(buffer->buffer, (uint32_t) buffer->length, &newHeaderList, nghttp2HdInflater);

    TEST_ASSERT_NOT_NULL(newHeaderList);

    nghttp2_hd_inflate_del(nghttp2HdInflater);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    http2HeaderListDeallocate(newHeaderList);
    http2HeaderListDeallocate(headerList);
    http2BufferDeallocate(buffer);
}
