/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NSHTTP2_HEADERS_H
#define NSHTTP2_HEADERS_H

void test_http2_header_field_allocate(void);

void test_http2_header_field_deallocate(void);

void test_http2_header_field_to_nv(void);

void test_http2_headers_to_nv_array(void);

void test_http2_header_allocate(void);

void test_http2_header_list_deallocate(void);

void test_http2_header_from_nghttp2_nv(void);

void test_http2_headers_decode(void);

void test_http2_headers_decode_differential_compression(void);

void test_http2_headers_encode(void);

void test_http2_nv_fields_deallocate(void);

void test_http2_nv_array_deallocate(void);

void test_http2_header_list_join(void);

#endif /* NSHTTP2_HEADERS_H */
