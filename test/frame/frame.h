/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NSHTTP2_FRAME_H
#define NSHTTP2_FRAME_H

void test_http2_is_non_zero_header_buffer(void);

void test_http2_frame_header_to_bytes(void);

void test_http2_frame_header_from_bytes(void);

void test_http2_frame_from_bytes_header_set_data_frame(void);

void test_http2_frame_from_bytes_header_set_headers_frame(void);

void test_http2_frame_from_bytes_header_set_priority_frame(void);

void test_http2_frame_from_bytes_header_set_rst_stream_frame(void);

void test_http2_frame_from_bytes_header_set_settings_frame(void);

void test_http2_frame_from_bytes_header_set_push_promise_frame(void);

void test_http2_frame_from_bytes_header_set_ping_frame(void);

void test_http2_frame_from_bytes_header_set_goaway_frame(void);

void test_http2_frame_from_bytes_header_set_window_update_frame(void);

void test_http2_frame_from_bytes_header_set_continuation_frame(void);

void test_http2_frame_from_bytes_data_frame(void);

void test_http2_frame_from_bytes_headers_frame(void);

void test_http2_frame_from_bytes_priority_frame(void);

void test_http2_frame_from_bytes_rst_stream_frame(void);

void test_http2_frame_from_bytes_settings_frame(void);

void test_http2_frame_from_bytes_push_promise_frame(void);

void test_http2_frame_from_bytes_ping_frame(void);

void test_http2_frame_from_bytes_goaway_frame(void);

void test_http2_frame_from_bytes_window_update_frame(void);

void test_http2_frame_from_bytes_continuation_frame(void);

void test_http2_frame_to_bytes_frame_with_allocation(void);

void test_http2_is_valid_connection_preface(void);

void test_http2_frame_data_frame_payload_allocate(void);

void test_http2_frame_headers_frame_payload_allocate(void);

void test_http2_frame_priority_frame_payload_allocate(void);

void test_http2_frame_rst_stream_frame_payload_allocate(void);

void test_http2_frame_settings_frame_payload_allocate(void);

void test_http2_frame_push_promise_frame_payload_allocate(void);

void test_http2_frame_ping_frame_payload_allocate(void);

void test_http2_frame_goaway_frame_payload_allocate(void);

void test_http2_frame_goaway_frame_payload_allocate_debug_data(void);

void test_http2_frame_window_update_frame_payload_allocate(void);

void test_http2_frame_continuation_frame_payload_allocate(void);

void test_http2_frame_type_name(void);

void test_http2_frame_is_control_frame(void);

void test_http2_frame_is_non_control_frame(void);

void test_http2_frame_has_priority(void);

void test_http2_frame_is_last_header(void);

#endif /* NSHTTP2_FRAME_H */
