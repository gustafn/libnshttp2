/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include "frame_list.h"
#include <nshttp2/frame_list.h>
#include <nshttp2/headers.h>
#include <nshttp2/unity/unity.h>
#include <nshttp2/utility/nghttp2.h>

void test_http2_frame_list_allocate(void) {
    Http2FrameHeaderT header;
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    Http2FrameT frame;
    Http2FrameListT *frame_list;

    header.type = DATA_FRAME_TYPE;
    header.flags = PADDED_FRAME_FLAG;
    header.length = 14u;

    frame.header = header;
    frame.payload.http2DataFramePayload = data_frame_payload;

    http2FrameListAllocate(&frame_list, &frame);

    TEST_ASSERT_NOT_NULL(frame_list->head);
    TEST_ASSERT_NOT_NULL(frame_list->tail);
    TEST_ASSERT_EQUAL(&frame, frame_list->head->http2Frame);
    TEST_ASSERT_EQUAL(&frame, frame_list->tail->http2Frame);
    TEST_ASSERT_NULL(frame_list->head->next);
    TEST_ASSERT_NULL(frame_list->head->previous);
    TEST_ASSERT_NULL(frame_list->tail->next);
    TEST_ASSERT_NULL(frame_list->tail->previous);
    TEST_ASSERT_EQUAL(1u, frame_list->length);
    TEST_ASSERT_EQUAL(14u + HTTP2_FRAME_HEADER_SIZE, frame_list->requiredBufferSize);

    http2FrameListDeallocate(frame_list, 0u);
}

void test_http2_frame_list_append(void) {
    Http2FrameHeaderT header;
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    Http2FrameT frame;
    Http2FrameListT *frame_list;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    Http2FrameT headers_frame;

    header.type = DATA_FRAME_TYPE;
    header.flags = PADDED_FRAME_FLAG;
    header.length = 14u;

    frame.header = header;
    frame.payload.http2DataFramePayload = data_frame_payload;

    http2FrameListAllocateEmpty(&frame_list);

    http2FrameListAppend(frame_list, &frame);

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerPayload = buffer->buffer;
    payload.priority.dependency = 20u;
    payload.priority.weight = 8u;
    payload.priority.exclusive = true;

    headers_frame.header.length = buffer->length;
    headers_frame.header.flags = END_HEADERS_FRAME_FLAG | PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    headers_frame.header.streamId = 4u;
    headers_frame.header.type = HEADERS_FRAME_TYPE;
    headers_frame.payload.http2HeadersFramePayload = &payload;

    http2FrameListAppend(frame_list, &headers_frame);

    TEST_ASSERT_NOT_NULL(frame_list->head);
    TEST_ASSERT_NOT_NULL(frame_list->tail);
    TEST_ASSERT_EQUAL(&frame, frame_list->head->http2Frame);
    TEST_ASSERT_EQUAL(&headers_frame, frame_list->tail->http2Frame);
    TEST_ASSERT_EQUAL(&headers_frame, frame_list->head->next->http2Frame);
    TEST_ASSERT_NOT_NULL(frame_list->head->next);
    TEST_ASSERT_NULL(frame_list->head->previous);
    TEST_ASSERT_NULL(frame_list->tail->next);
    TEST_ASSERT_EQUAL(&frame, frame_list->tail->previous->http2Frame);
    TEST_ASSERT_EQUAL(2u, frame_list->length);
    TEST_ASSERT_EQUAL(14u + HTTP2_FRAME_HEADER_SIZE * 2 + buffer->length, frame_list->requiredBufferSize);

    http2BufferDeallocate(buffer);
    http2FrameListDeallocate(frame_list, 0u);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
}

void test_http2_frame_list_allocate_empty(void) {
    Http2FrameListT *frame_list;
    http2FrameListAllocateEmpty(&frame_list);

    TEST_ASSERT_NULL(frame_list->head);
    TEST_ASSERT_NULL(frame_list->tail);
    TEST_ASSERT_EQUAL(0u, frame_list->length);

    http2FrameListDeallocate(frame_list, 0u);
}

void test_http2_frame_list_join(void) {
    Http2FrameHeaderT header;
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    Http2FrameT frame;
    Http2FrameListT *frame_list;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    Http2FrameT headers_frame;
    Http2FrameListT *second_frame_list;

    header.type = DATA_FRAME_TYPE;
    header.flags = PADDED_FRAME_FLAG;
    header.length = 14u;

    frame.header = header;
    frame.payload.http2DataFramePayload = data_frame_payload;

    http2FrameListAllocateEmpty(&frame_list);

    http2FrameListAppend(frame_list, &frame);

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerPayload = buffer->buffer;
    payload.priority.dependency = 20u;
    payload.priority.weight = 8u;
    payload.priority.exclusive = true;

    headers_frame.header.length = (uint32_t) buffer->length;
    headers_frame.header.flags = END_HEADERS_FRAME_FLAG | PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    headers_frame.header.streamId = 4u;
    headers_frame.header.type = HEADERS_FRAME_TYPE;
    headers_frame.payload.http2HeadersFramePayload = &payload;

    http2FrameListAppend(frame_list, &headers_frame);

    http2FrameListAllocate(&second_frame_list, &frame);

    http2FrameListJoin(second_frame_list, frame_list);

    TEST_ASSERT_EQUAL(second_frame_list->head->http2Frame, &frame);
    TEST_ASSERT_EQUAL(second_frame_list->head->next->http2Frame, &frame);
    TEST_ASSERT_EQUAL(second_frame_list->head->next->next->http2Frame, &headers_frame);
    TEST_ASSERT_EQUAL(3u, second_frame_list->length);
    TEST_ASSERT_EQUAL(second_frame_list->tail->http2Frame, &headers_frame);
    TEST_ASSERT_EQUAL(second_frame_list->head->http2Frame, &frame);

    http2BufferDeallocate(buffer);
    http2FrameListDeallocate(second_frame_list, 0u);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
}

void test_http2_frame_list_to_bytes(void) {
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    Http2FrameT first_frame;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    Http2FrameT second_frame;
    Http2FrameListT *frame_list;
    Http2BufferT *frame_list_buffer;
    uint8_t first_frame_buffer[14u + HTTP2_FRAME_HEADER_SIZE];
    uint8_t *second_frame_buffer;

    first_frame.header.type = DATA_FRAME_TYPE;
    first_frame.header.flags = DEFAULT_FRAME_FLAG;
    first_frame.header.length = 5;
    first_frame.header.streamId = 12u;
    first_frame.payload.http2DataFramePayload = data_frame_payload;

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.length = 2u;
    header_list.head = &host_element;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerPayload = buffer->buffer;
    payload.priority.dependency = 20u;
    payload.priority.weight = 8u;
    payload.priority.exclusive = true;

    second_frame.header.length = (uint32_t) buffer->length;
    second_frame.header.flags = END_HEADERS_FRAME_FLAG | PADDED_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    second_frame.header.streamId = 4u;
    second_frame.header.type = HEADERS_FRAME_TYPE;
    second_frame.payload.http2HeadersFramePayload = &payload;

    http2FrameListAllocate(&frame_list, &first_frame);
    http2FrameListAppend(frame_list, &second_frame);

    http2FrameListToBytes(&frame_list_buffer, frame_list);

    http2FrameToBytes(&first_frame, first_frame_buffer, NULL);

    second_frame_buffer = http2Malloc(second_frame.header.length + HTTP2_FRAME_HEADER_SIZE);
    http2FrameToBytes(&second_frame, second_frame_buffer, NULL);

    TEST_ASSERT_EQUAL(0, memcmp(first_frame_buffer,
                                frame_list_buffer->buffer,
                                first_frame.header.length + HTTP2_FRAME_HEADER_SIZE));

    TEST_ASSERT_EQUAL(0, memcmp(second_frame_buffer,
                                frame_list_buffer->buffer + first_frame.header.length + HTTP2_FRAME_HEADER_SIZE,
                                second_frame.header.length + HTTP2_FRAME_HEADER_SIZE));

    http2BufferDeallocate(frame_list_buffer);
    http2FrameListDeallocate(frame_list, 0u);
    http2BufferDeallocate(buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    http2Free(second_frame_buffer);
}

void test_http2_frame_list_from_bytes(void) {
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    Http2FrameT first_frame;
    Http2HeaderFieldT user_agent;
    Http2HeaderListElementT user_agent_element;
    Http2HeaderFieldT host;
    Http2HeaderListElementT host_element;
    Http2HeaderListT header_list;
    nghttp2_hd_deflater *nghttp2HdDeflater = NULL;
    nghttp2_hd_inflater *nghttp2HdInflater = NULL;
    Http2BufferT *buffer;
    Http2HeadersFramePayloadT payload;
    Http2FrameT second_frame;
    Http2FrameListT *frame_list;
    Http2BufferT *frame_list_buffer;
    uint8_t first_frame_buffer[14u + HTTP2_FRAME_HEADER_SIZE] = {0};
    uint8_t *second_frame_buffer;
    Http2FrameListT *second_frame_list;
    size_t parsed_buffer;

    first_frame.header.type = DATA_FRAME_TYPE;
    first_frame.header.flags = DEFAULT_FRAME_FLAG;
    first_frame.header.length = 5u;
    first_frame.header.streamId = 7u;
    first_frame.payload.http2DataFramePayload = data_frame_payload;

    user_agent.name = (uint8_t *) "user-agent";
    user_agent.nameLength = strlen("user-agent");
    user_agent.value = (uint8_t *) "curl/7.68.0";
    user_agent.valueLength = strlen("curl/7.68.0");

    user_agent_element.next = NULL;
    user_agent_element.http2HeaderField = &user_agent;

    host.name = (uint8_t *) "host";
    host.nameLength = strlen("host");
    host.value = (uint8_t *) "www.wu.ac.at";
    host.valueLength = strlen("www.wu.ac.at");

    host_element.next = &user_agent_element;
    host_element.http2HeaderField = &host;

    header_list.head = &host_element;
    header_list.length = 2u;
    header_list.tail = &user_agent_element;

    ngHttp2GetDeflater(&nghttp2HdDeflater, 4096u);

    ngHttp2GetInflater(&nghttp2HdInflater);

    http2HeadersEncode(&header_list,
                       &buffer,
                       nghttp2HdDeflater);

    payload.headerPayload = buffer->buffer;
    payload.priority.dependency = 20u;
    payload.priority.weight = 8u;
    payload.priority.exclusive = true;

    second_frame.header.length = (uint32_t) buffer->length;
    second_frame.header.flags = END_HEADERS_FRAME_FLAG | PRIORITY_FRAME_FLAG;
    second_frame.header.streamId = 4u;
    second_frame.header.type = HEADERS_FRAME_TYPE;
    second_frame.payload.http2HeadersFramePayload = &payload;

    http2FrameListAllocate(&frame_list, &first_frame);
    http2FrameListAppend(frame_list, &second_frame);

    http2FrameListToBytes(&frame_list_buffer, frame_list);

    http2FrameToBytes(&first_frame, first_frame_buffer, NULL);

    second_frame_buffer = http2Malloc(second_frame.header.length + HTTP2_FRAME_HEADER_SIZE);
    http2FrameToBytes(&second_frame, second_frame_buffer, NULL);

    TEST_ASSERT_EQUAL(0, memcmp(first_frame_buffer,
                                frame_list_buffer->buffer,
                                first_frame.header.length + HTTP2_FRAME_HEADER_SIZE));

    TEST_ASSERT_EQUAL(0, memcmp(second_frame_buffer,
                                frame_list_buffer->buffer + first_frame.header.length + HTTP2_FRAME_HEADER_SIZE,
                                second_frame.header.length + HTTP2_FRAME_HEADER_SIZE));

    http2FrameListFromBytes(frame_list_buffer->buffer,
                            false, frame_list_buffer->length,
                            &second_frame_list,
                            &parsed_buffer);

    TEST_ASSERT_EQUAL(frame_list->head->http2Frame->header.length,
                      second_frame_list->head->http2Frame->header.length);
    TEST_ASSERT_EQUAL(frame_list->head->http2Frame->header.flags,
                      second_frame_list->head->http2Frame->header.flags);
    TEST_ASSERT_EQUAL(frame_list->head->http2Frame->header.type,
                      second_frame_list->head->http2Frame->header.type);
    TEST_ASSERT_EQUAL(frame_list->head->http2Frame->header.streamId,
                      second_frame_list->head->http2Frame->header.streamId);

    TEST_ASSERT_EQUAL(0, memcmp(frame_list->head->http2Frame->payload.http2DataFramePayload,
                                second_frame_list->head->http2Frame->payload.http2DataFramePayload,
                                frame_list->head->http2Frame->header.length));

    http2FrameListDeallocate(frame_list, 0u);
    http2BufferDeallocate(buffer);
    http2FrameListDeallocate(second_frame_list, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
    http2BufferDeallocate(frame_list_buffer);
    nghttp2_hd_deflate_del(nghttp2HdDeflater);
    nghttp2_hd_inflate_del(nghttp2HdInflater);
    http2Free(second_frame_buffer);
}

void test_http2_frame_list_element_remove(void) {
    uint8_t data_frame_payload[5] = {'t', 'e', 's', 't', 's'};
    Http2FrameT first_frame;
    Http2FrameT second_frame;
    Http2FrameListT *frame_list;

    first_frame.header.type = DATA_FRAME_TYPE;
    first_frame.header.flags = DEFAULT_FRAME_FLAG;
    first_frame.header.length = 14u;
    first_frame.header.streamId = 3u;
    first_frame.payload.http2DataFramePayload = data_frame_payload;

    second_frame.header.type = DATA_FRAME_TYPE;
    second_frame.header.flags = DEFAULT_FRAME_FLAG;
    second_frame.header.length = 14u;
    second_frame.header.streamId = 3u;
    second_frame.payload.http2DataFramePayload = data_frame_payload;

    http2FrameListAllocate(&frame_list, &first_frame);
    http2FrameListAppend(frame_list, &second_frame);

    http2FrameListElementRemove(frame_list->head, frame_list, false);

    TEST_ASSERT_EQUAL(1u, frame_list->length);
    TEST_ASSERT_EQUAL(frame_list->requiredBufferSize, 14u + HTTP2_FRAME_HEADER_SIZE);

    http2FrameListDeallocate(frame_list, 0u);
}
