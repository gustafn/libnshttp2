/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NS_HTTP2_LIBS_BUFFER_H
#define NS_HTTP2_LIBS_BUFFER_H

#include <nshttp2/stddef.h>
#include <nshttp2/utility/memory.h>
#include <nshttp2/return_code.h>

typedef struct Http2Buffer {
    uint8_t *buffer;
    size_t length;
} Http2BufferT;

/**
 * Allocates an http2 buffer structure on the heap.
 * The buffer is allocated with the provided initial length.
 * For an empty buffer set the initial length to 0u.
 * @param buffer Buffer pointer into which to place the allocated buffer.
 * @param initialLength Initial length of the buffer.
 * @return Return codes:
 * - HTTP2_SUCCESS - Everything went fine.
 * - HTTP2_NO_MEMORY - There is not enough contiguous memory left, allocation failed.
 */
Http2ReturnCodeT http2BufferAllocate(Http2BufferT **buffer,
                                     size_t initialLength);

/**
 * Deallocates the buffer container and the buffer inside.
 */
void http2BufferDeallocate(Http2BufferT *buffer);

/**
 * Extends an existing buffer. Note that the additional memory is not zeroed out.
 * @param http2Buffer Buffer to extend.
 * @param length Length of the buffer extension.
 * Return codes:
 * - HTTP2_SUCCESS - Everything went fine, the buffer has been extended.
 * - HTTP2_NO_MEMORY -There is not enough contiguous memory left, allocation failed.
 */
Http2ReturnCodeT http2BufferExtend(Http2BufferT *http2Buffer, size_t length);

/**
 * Extends an existing buffer, and copies the contents of the provided buffer to the extended block.
 * @param http2Buffer Buffer to extend.
 * @param buffer Buffer to append to the existing buffer.
 * @param length Length of the buffer being appended.
 * Return codes:
 * - HTTP2_SUCCESS - everything went fine, the buffer has been extended
 * and the provided buffer contents copied.
 * - HTTP2_NO_MEMORY - There is not enough contiguous memory left, allocation failed.
 */
Http2ReturnCodeT http2BufferAppend(Http2BufferT *http2Buffer,
                                   uint8_t *buffer,
                                   size_t length);

/**
 * Extends an existing buffer, and copies the contents of the provided buffer to the start of the block block.
 * @param http2Buffer Buffer to extend.
 * @param buffer Buffer to append to the existing buffer.
 * @param length Length of the buffer being appended.
 * Return codes:
 * - HTTP2_SUCCESS - everything went fine, the buffer has been extended
 * and the provided buffer contents copied.
 * - HTTP2_NO_MEMORY - There is not enough contiguous memory left, allocation failed.
 */
Http2ReturnCodeT http2BufferPrepend(Http2BufferT *http2Buffer,
                                    uint8_t *buffer,
                                    size_t length);

/**
 * Removes the offset from the start of the given buffer.
 * The existing buffer is move
 * @param http2Buffer Buffer from which to remove the offset.
 * @param offset Offset length.
 */
void http2BufferMoveByOffset(Http2BufferT *http2Buffer, size_t offset);

/**
 * Reset buffer. This frees the existing buffer, sets it to NULL and sets the length to 0.
 * @param http2Buffer Buffer to reset.
 */
void http2BufferReset(Http2BufferT *http2Buffer);

#endif /* NS_HTTP2_LIBS_BUFFER_H */

