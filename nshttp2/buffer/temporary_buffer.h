/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NSHTTP2_TEMPORARY_BUFFER_H
#define NSHTTP2_TEMPORARY_BUFFER_H

#include <stdlib.h>
#include <nshttp2/stddef.h>
#include <nshttp2/return_code.h>

typedef struct Http2TemporaryBuffer {
    uint8_t *memoryBuffer;
    size_t maxIncomingDataInMemory;
    size_t bufferLength;

#ifdef __unix__
    uint8_t *mappedBuffer;
    char *temporaryFile;
    int temporaryFileDescriptor;
#endif
} Http2TemporaryBufferT;

/**
 * Resets the temporary buffer for new use. This sets it to the state after
 * a fresh allocation.
 * @param http2TemporaryBuffer Temporary buffer pointer.
 */
void http2TemporaryBufferReset(Http2TemporaryBufferT *http2TemporaryBuffer);

/**
 * Allocates a temporary buffer on the heap.
 * @param http2TemporaryBuffer Temporary buffer pointer.
 * @param maxIncomingDataInMemory Maximum bytes to keep in memory before storing on
 * the file system.
 * @return Following return values are possible:
 * HTTP2_NO_MEMORY - Not enough memory on the heap available.
 * HTTP2_SUCCESS - Everything went fine.
 */
Http2ReturnCodeT http2TemporaryBufferAllocate(Http2TemporaryBufferT **http2TemporaryBuffer,
                                              size_t maxIncomingDataInMemory);

Http2ReturnCodeT http2TemporaryBufferAppend(Http2TemporaryBufferT *http2TemporaryBuffer,
                                            uint8_t *buffer,
                                            size_t bufferLength,
                                            const char *temporaryFileTemplate);

Http2ReturnCodeT http2TemporaryBufferGetData(Http2TemporaryBufferT *http2TemporaryBuffer,
                                             uint8_t **buffer,
                                             size_t *bufferLength);

void http2TemporaryBufferDeallocate(Http2TemporaryBufferT *http2TemporaryBuffer);

Http2ReturnCodeT http2TemporaryBufferCloseData(Http2TemporaryBufferT *http2TemporaryBuffer);

bool http2TemporaryBufferHasData(Http2TemporaryBufferT *http2TemporaryBuffer);

#endif /* NSHTTP2_TEMPORARY_BUFFER_H */
