/*
MIT License
-----------

Copyright (c) 2021 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NSHTTP2_STDDEF_H
#define NSHTTP2_STDDEF_H

/* GNU GCC helper macros */
#if defined(__GNUC__) || defined(__clang__)
#  define UNUSED(x) UNUSED_ ## x __attribute__((__unused__))
#else
#  define UNUSED(x) UNUSED_ ## x
#endif

#if defined(__GNUC__) || defined(__clang__)
#  define UNUSED_FUNCTION(x) __attribute__((__unused__)) UNUSED_ ## x
#else
#  define UNUSED_FUNCTION(x) UNUSED_ ## x
#endif
/* GNU GCC helper macros END */

#include <nshttp2/config.h>

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L && HAVE_STDINT_H && HAVE_STDBOOL_H

/* Define const restrict specifier */
#define HTTP2_CONST_RESTRICT_SPECIFIER const restrict
#define HTTP2_INLINE_SPECIFIER inline

#include <stdint.h>
#include <stdbool.h>

#else

/* Define const restrict specifier */
#define HTTP2_CONST_RESTRICT_SPECIFIER const
#define HTTP2_INLINE_SPECIFIER

/* Define bool for C90 */

#ifndef bool
#define bool unsigned char
#define true 1
#define false 0
#endif

#ifdef HAVE_STDINT_H

#include <stdint.h>

#else

/* Define stdints for C90 */
#include <limits.h>

#define UINT8_MAX 255u
#define UINT16_MAX 65535u
#define UINT32_MAX 4294967295u
#define UINT64_MAX 18446744073709551615u

#define INT8_MAX 127
#define INT16_MAX 32767
#define INT32_MAX 2147483647
#define INT64_MAX 9223372036854775807L

#define INT8_MIN -128
#define INT16_MIN -32768
#define INT32_MIN -2147483648
#define INT64_MIN -9223372036854775808L

#if UCHAR_MAX == UINT8_MAX
typedef unsigned char uint8_t;
typedef unsigned char uint_fast8_t;
#elif USHRT_MAX == UINT8_MAX
typedef unsigned short uint8_t;
typedef unsigned short uint_fast8_t;
#endif

#if CHAR_MAX == INT8_MAX
typedef char int8_t;
typedef char int_fast8_t;
#elif SHRT_MAX == INT8_MAX
typedef char int8_t;
typedef char int_fast8_t;
#endif

#if USHRT_MAX == UINT16_MAX
typedef unsigned short uint16_t;
typedef unsigned short uint_fast16_t;
#elif UINT_MAX == UINT16_MAX
typedef unsigned int uint16_t;
typedef unsigned int uint_fast16_t;
#elif ULONG_MAX == UINT16_MAX
typedef unsigned long uint16_t;
typedef unsigned long uint_fast16_t;
#endif

#if SHRT_MAX == INT16_MAX
typedef short int16_t;
typedef short int_fast16_t;
#elif INT_MAX == INT16_MAX
typedef int int16_t;
typedef int int_fast16_t;
#elif LONG_MAX == INT16_MAX
typedef long int16_t;
typedef long int_fast16_t;
#endif

#if UINT_MAX == UINT32_MAX
typedef unsigned int uint32_t;
typedef unsigned int uint_fast32_t;
#elif ULONG_MAX == UINT32_MAX
typedef unsigned long uint32_t;
typedef unsigned long uint_fast32_t;
#elif sizeof(size_t) == 32
typedef size_t uin32_t;
typedef size_t uint_fast32_t;
#endif

#if INT_MAX == INT32_MAX
typedef int int32_t;
typedef int int_fast32_t;
#elif LONG_MAX == INT32_MAX
typedef long int32_t;
typedef long int_fast32_t;
#else sizeof(ssize_t) == 32
typedef ssize_t in32_t;
typedef ssize_t int_fast32_t;
#endif

#endif

#endif

#ifdef __unix__

#include <unistd.h>

#elif defined(__WIN32)

#include <io.h>

#endif

#endif /* NSHTTP2_STDDEF_H */
