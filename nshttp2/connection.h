/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * Provides functions and structures needed
 * to parse frames from a buffer into a list.
 */

#ifndef NS_HTTP2_LIBS_CONNECTION_STATE_H
#define NS_HTTP2_LIBS_CONNECTION_STATE_H

#define CLIENT_STREAM_ID_MAX 2147483647u
#define SERVER_STREAM_ID_MAX 2147483646u

#include <pthread.h>
#include <nshttp2/frame_payload.h>
#include <nshttp2/frame.h>
#include <nshttp2/stream.h>
#include <nshttp2/buffer/buffer.h>
#include <nshttp2/stream_hashmap.h>
#include <nshttp2/stddef.h>

typedef struct Http2ConnectionSettings {
    size_t maxIncomingDataInMemory;
    char *temporaryFileTemplate;
    void (*closeConnectionFunction)(void); /* Function to invoke during connection closing. */
    Http2SettingsFramePayloadT receivedSettings; /* Settings frame received */
    bool isClient; /* Determines if the connection is of type server or client. */
    Http2SettingsFramePayloadT sentSettings; /* Settings frame sent */
} Http2ConnectionSettingsT;

typedef struct Http2Connection {
    /* Mutex for the HTTP/2 connection level */
    pthread_mutex_t mutex;

    /* Map to quickly resolve stream ids */
    StreamHashmapT *streamIdToStreamMap;

    /* HPACK data */
    nghttp2_hd_inflater *inflater;
    nghttp2_hd_deflater *deflater;

    /* Fixed connection settings, set during connection initiation. */
    Http2SettingsFramePayloadT receivedSettings;
    Http2SettingsFramePayloadT sentSettings;

    bool isClient;

    /* Settings below are ones that change during the connection lifetime */
    int_fast32_t remainingSendWindow; /* Remaining flow control available to send */
    int_fast32_t remainingReceiveWindow; /* Remaining flow control available to receive */
    uint_fast32_t lastSentStreamId; /* Last sent stream id */
    uint_fast32_t lastReceivedStreamId; /* Last received stream id */
    uint_fast32_t activeStreams; /* Count of currently active streams */

    size_t maxIncomingDataInMemory;
    char *temporaryFileTemplate;

    void (*closeConnectionFunction)(void); /* Function to invoke during connection closing. */
} Http2ConnectionT;

/**
 * Allocates a HTTP/2 connection and allocates it on the heap, this should be done once
 * settings frames have been exchanged (received and sent) between the connection nodes (client and server).
 * @param http2Connection HTTP/2 connection pointer
 * where to allocate the initial connection state.
 * @param connectionSettings Settings for the connection.
 * @return Return codes:
 * - HTTP2_NO_MEMORY - Could not allocate the structure on the heap.
 * - HTTP2_SUCCESS - Everything went fine.
 */
Http2ReturnCodeT http2ConnectionAllocate(Http2ConnectionT **http2Connection,
                                         Http2ConnectionSettingsT *connectionSettings);

/**
 * Deallocates a HTTP/2 connection and all its elements, additionally closes the
 * connection if the function was provided.
 * @param http2Connection HTTP/2 connection DTO.
 */
void http2ConnectionDeallocateAndClose(Http2ConnectionT *http2Connection);

/**
 * Processes a frame both on the connection and stream level and updates the state accordingly.
 * @param http2Connection HTTP/2 connection DTO.
 * @param frame New incoming or outgoing HTTP/2 frame.
 * @param sent Boolean indicating if the frame is being sent or received on this connection.
 * @return Return codes:
 * - HTTP2_SUCCESS - Everything went fine, the frame has been processed.
 * - HTTP2_UNSUPPORTED_FRAME_TYPE - Unsupported frame type provided.
 * - HTTP2_PROTOCOL_ERROR - The frame results in a protocol error.
 * - HTTP2_NO_MEMORY - Not enough memory on the heap.
 */
Http2ReturnCodeT http2ConnectionProcessFrame(Http2ConnectionT *http2Connection,
                                             Http2FrameT *frame,
                                             bool sent);

/**
 * Processes a frame both on the connection and stream level and updates the state accordingly.
 * @param http2Connection HTTP/2 connection DTO.
 * @param frameList List of frames to process.
 * @param sent Boolean indicating if the frame is being sent or received on this connection.
 * @return Return codes:
 * - HTTP2_SUCCESS - Everything went fine, the frame has been processed.
 * - HTTP2_PROTOCOL_ERROR - The frame results in a protocol error.
 * - HTTP2_NO_MEMORY - Not enough memory on the heap.
 */
Http2ReturnCodeT http2ConnectionProcessFrameList(Http2ConnectionT *http2Connection,
                                                 Http2FrameListT *frameList,
                                                 bool sent);

/**
 * Processes a control frame, this only affects the connection level, no stream is affected.
 * @param http2Connection HTTP/2 connection DTO.
 * @param frame New incoming or outgoing HTTP/2 frame.
 * @param sent Boolean indicating if the frame is being sent or received on this connection.
 * @return Return codes:
 * - HTTP2_SUCCESS - Everything went fine.
 * - HTTP2_PROTOCOL_ERROR - Protocol error encountered.
 */
Http2ReturnCodeT http2ConnectionProcessNonStreamFrame(Http2ConnectionT *http2Connection,
                                                      Http2FrameT *frame,
                                                      bool sent);

/**
 * Processes a non-control frame, this only affects the connection level, no stream is affected.
 * @param http2Connection HTTP/2 connection DTO.
 * @param frame New incoming or outgoing HTTP/2 frame.
 * @param sent Boolean indicating if the frame is being sent or received on this connection.
 * @return Return codes:
 * - HTTP2_SUCCESS - Everything went fine, the frame has been processed.
 * - HTTP2_UNSUPPORTED_FRAME_TYPE - Unsupported frame type provided.
 * - HTTP2_PROTOCOL_ERROR - The frame results in a protocol error.
 * - HTTP2_NO_MEMORY - Not enough memory on the heap.
 * - HTTP2_STREAM_ID_TOO_LARGE - Stream id exceeds allowed maximum.
 */
Http2ReturnCodeT http2ConnectionProcessStreamFrame(Http2ConnectionT *http2Connection,
                                                   Http2FrameT *frame,
                                                   bool sent);

/**
 * First clears the incoming and outgoing buffers,
 * then puts a goaway frame with the given header into the outgoing buffer.
 * @param http2Connection HTTP/2 connection DTO.
 * @param errorCode Error code to put into the goaway buffer.
 * @return Return codes:
 * - HTTP2_SUCCESS - Everything went fine, the frame has been processed.
 * - HTTP2_NO_MEMORY - Not enough memory on the heap.
 */
Http2ReturnCodeT http2ConnectionCreateGoaway(Http2ConnectionT *http2Connection,
                                             uint32_t errorCode,
                                             uint8_t *buffer);

/**
 * Processes a data frame, this affects both connection and stream level.
 * @param http2Connection HTTP/2 connection DTO.
 * @param frame New incoming or outgoing HTTP/2 frame.
 * @param sent Boolean indicating if the frame is being sent or received on this connection.
 * Return codes:
 * - HTTP2_PROTOCOL_ERROR - Protocol error encountered.
 * - HTTP2_SUCCESS - Everything went fine.
 */
Http2ReturnCodeT http2ConnectionProcessDataFrame(Http2ConnectionT *http2Connection,
                                                 Http2FrameT *frame,
                                                 Http2StreamT *stream,
                                                 bool sent);

Http2ReturnCodeT http2ConnectionProcessPriority(Http2ConnectionT *http2Connection,
                                                Http2FrameT *frame);

Http2ReturnCodeT http2ConnectionProcessHeadersFrame(Http2ConnectionT *http2Connection,
                                                    Http2FrameT *frame,
                                                    bool sent);

Http2ReturnCodeT http2ConnectionProcessContinuationFrame(Http2ConnectionT *http2Connection,
                                                         Http2FrameT *frame,
                                                         bool sent);

Http2ReturnCodeT http2ConnectionCreateStream(Http2ConnectionT *http2Connection,
                                             uint32_t streamId,
                                             uint8_t weight,
                                             Http2StreamT **stream);

Http2ReturnCodeT http2ConnectionGetOrCreateStream(Http2ConnectionT *http2Connection,
                                                  uint32_t streamId,
                                                  uint8_t weight,
                                                  Http2StreamT **stream);

Http2ReturnCodeT http2ConnectionGetStream(Http2ConnectionT *http2Connection,
                                          uint32_t streamId,
                                          Http2StreamT **stream);

void http2ConnectionRemoveAndDeallocateStream(Http2ConnectionT *http2Connection,
                                              uint32_t streamId);

Http2ReturnCodeT http2ConnectionProcessWindowUpdateFrameOnStream(Http2ConnectionT *http2Connection,
                                                                 Http2FrameT *frame,
                                                                 bool sent);

Http2ReturnCodeT http2ConnectionProcessRstStreamFrame(Http2ConnectionT *http2Connection,
                                                      Http2FrameT *frame);

/**
 * Determines the state of the stream id, in particular if the stream id of the frame is
 * valid for the connection.
 * @param http2Connection HTTP/2 connection DTO.
 * @param frame Frame DTO.
 * @param sent Boolean indicating if the frame is being sent or received on the connection.
 * @return Return codes:
 * - HTTP2_SUCCESS - The stream id is valid.
 * - HTTP2_STREAM_ID_TOO_LARGE - The stream id is too large, beyond the maximum stream id.
 * The connection needs to be closed at this point.
 */
Http2ReturnCodeT http2ConnectionStreamIdState(const Http2ConnectionT *http2Connection,
                                              const Http2FrameT *frame,
                                              bool sent);

Http2ReturnCodeT http2ConnectionGetFramedStreamData(Http2StreamT *http2Stream,
                                                    Http2FrameListT **frameList,
                                                    Http2ConnectionT *http2Connection,
                                                    size_t chunkSize,
                                                    bool isEndOfStream);

Http2ReturnCodeT http2ConnectionGetHpackInflater(Http2ConnectionT *http2Connection);

Http2ReturnCodeT http2ConnectionGetHpackDeflater(Http2ConnectionT *http2Connection);

/**
 * Decodes headers found in the incoming stream headers buffer.
 * It clears the headers buffer once they are decoded and put into the headerList.
 * @param http2Stream HTTP/2 stream DTO.
 * @param headerList Header list into which to put the decoded header buffer.
 * @return Returns the following return codes:
 * - HTTP2_NO_MEMORY - No memory available for headerList allocation.
 * - HTTP2_SUCCESS - Everything is fine.
 */
Http2ReturnCodeT http2ConnectionStreamDecodeHeaders(Http2StreamT *http2Stream,
                                                    Http2HeaderListT **headerList,
                                                    Http2ConnectionT *http2Connection);

/**
 * Allocates and fills the buffer based on the header_table_size, max_frame_size
 * from the settings frame.
 * @param http2HeaderList HTTP/2 header list DTO.
 * @param buffer Buffer pointer where to put the allocated deflated data.
 * @return Following return values are possible:
 * HTTP2_NO_MEMORY - Not enough memory on the heap available.
 * HTTP2_BUFFER_INSUFFICIENT - Too many HTTP/2 headers provided, exceeds the maximum header size.
 * HTTP2_FAILURE - Deflation process has failed.
 * HTTP2_SUCCESS - Everything went fine.
 */
Http2ReturnCodeT http2ConnectionHeadersEncode(Http2HeaderListT *http2HeaderList,
                                              Http2BufferT **buffer,
                                              Http2ConnectionT *http2Connection);

/**
 * Retrieves the remaining send window for HTTP/2 stream. It retrieves the remaining connection
 * window and stream window and returns the smaller one.
 * @param http2Stream HTTP/2 stream DTO.
 * @param http2Connection HTTP/2 connection DTO.
 * @return Maximum data allowed to be sent at this moment on the stream.
 */
int_fast32_t http2ConnectionGetRemainingSendWindowForStream(Http2StreamT *http2Stream,
                                                            Http2ConnectionT *http2Connection);

#endif /* NS_HTTP2_LIBS_CONNECTION_STATE_H */

