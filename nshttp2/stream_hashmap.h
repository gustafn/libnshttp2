/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef HASHMAP_H_INCLUDED
#define HASHMAP_H_INCLUDED

#include <pthread.h>
#include <stdlib.h>
#include <nshttp2/return_code.h>
#include <nshttp2/klib/khash.h>
#include <nshttp2/stddef.h>

struct Http2Stream;

/* Initialize a KHASHMAP */
KHASH_MAP_INIT_INT(hashmap, struct Http2Stream *)

/**
 * Hashmap used to hold 1:1 mappings of stream ids and streams.
 */
typedef struct StreamHashmap {
    pthread_mutex_t mutex;
    kh_hashmap_t *hashmap;
} StreamHashmapT;

/**
 * Allocates a stream hashmap with the given start_size.
 * @param streamHashmap Stream hashmap pointer into which to put the allocated hashmap.
 * @return Return codes:
 * - HTTP2_NO_MEMORY - Not enough memory to allocate the structure.
 * - HTTP2_SUCCESS - Structure allocated.
 * - HTTP2_INTERNAL_ERROR - Pthread could not initialize mutex.
 */
Http2ReturnCodeT streamHashmapAllocate(StreamHashmapT **streamHashmap);

/**
 * Inserts a new stream pointer into the stream hashmap, the streamId is the key for the stream.
 * @param streamHashmap Stream hashmap into which to insert the streamId and stream.
 * @param stream Stream pointer to insert into the hashmap. The key is taken from the stream.
 */
void streamHashmapInsert(StreamHashmapT *streamHashmap,
                         struct Http2Stream *stream);

void streamHashmapInsertWithStreamId(StreamHashmapT *streamHashmap,
                                     uint32_t streamId,
                                     struct Http2Stream *stream);

/**
 * Remove stream id key and value in the hashmap.
 * @param streamHashmap Hashmap from which to remove the stream id.
 * @param streamId Stream id to remove from the stream hashmap.
 */
void streamHashmapRemove(StreamHashmapT *streamHashmap,
                         uint_fast32_t streamId);

/**
 * Retrieves a stream based on the streamId.
 * @param streamHashmap Stream hashmap in which to search for the stream id.
 * @param streamId Stream id to search for in the hashmap.
 * @param stream Stream pointer into which to put the stream.
 */
void streamHashmapGet(StreamHashmapT *streamHashmap,
                      uint_fast32_t streamId,
                      struct Http2Stream **stream);

/**
 * Retrieves a stream based on the streamId.
 * @param streamHashmap Stream hashmap in which to search for the stream id.
 * @param streamId Stream id to search for in the hashmap.
 * @param stream Stream pointer into which to put the stream.
 */
void streamHashmapGetWithoutLocking(StreamHashmapT *streamHashmap,
                                    uint_fast32_t streamId,
                                    struct Http2Stream **stream);


/**
 * Frees the stream hashmap from memory and all entries.
 * @param streamHashmap Stream hashmap to deallocate.
 */
void streamHashmapDeallocate(StreamHashmapT *streamHashmap);

#endif /* HASHMAP_H_INCLUDED */
