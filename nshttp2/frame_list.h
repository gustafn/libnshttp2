/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * Provides functions and structures needed
 * to parse frames from a buffer into a list.
 */

#ifndef NS_HTTP2_LIBS_FRAME_LIST_H
#define NS_HTTP2_LIBS_FRAME_LIST_H

#include <nshttp2/frame.h>
#include <nshttp2/buffer/buffer.h>
#include <nshttp2/stddef.h>

/**
 * Wrapper structure for Http2Frame elements,
 * used as a list element for Http2FrameList.
 */
typedef struct Http2FrameListElement {
    Http2FrameT *http2Frame;
    struct Http2FrameListElement *next;
    struct Http2FrameListElement *previous;
} Http2FrameListElementT;

/**
 * Singly-linked list containing Http2Frame elements
 * and length of the list.
 */
typedef struct Http2FrameList {
    Http2FrameListElementT *head;
    Http2FrameListElementT *tail;
    size_t length;
    size_t requiredBufferSize;
} Http2FrameListT;

typedef enum Http2FrameListReturnCode {
    /* Specifies a successful function execution */
    HTTP2_FRAME_LIST_SUCCESS,
    /* Specifies that the frame list is empty */
    HTTP2_FRAME_LIST_EMPTY,
    /* Specifies that the provided buffer is too short for any parsing */
    HTTP2_FRAME_LIST_BUFFER_TOO_SHORT,
    /* Specifies that the provided buffer contains only headers and not the complete body */
    HTTP2_FRAME_LIST_BUFFER_HAS_ONLY_HEADER,
    /* Specifies that the provided buffer has the header and not the whole body */
    HTTP2_FRAME_LIST_BUFFER_DOES_NOT_HAVE_WHOLE_BODY,
    /* Specifies that the whole buffer up to the padding has been processed */
    HTTP2_FRAME_LIST_BUFFER_PROCESSED,
    /* Specifies that the function could not allocate the required memory on the heap */
    HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY = -1
} Http2FrameListReturnCodeT;

/* Constants used to setup the bit mask for http2FrameListDeallocate */

/* Specifies that frames in the list should be deallocated */
#define HTTP2_FRAME_LIST_DEALLOCATE_FRAMES 0x1u
/* Specifies that the frame list is stack allocated */
#define HTTP2_FRAME_LIST_IS_STACK_ALLOCATED 0x2u

/**
 * Deallocates the frame list and if specified the frames themselves.
 * @param http2FrameList Frame list to deallocate.
 * @param settings Bit mask for the settings. Valid settings are:
 * HTTP2_FRAME_LIST_DEALLOCATE_FRAMES - determines if the frames should be deallocated.
 * HTTP2_FRAME_LIST_IS_STACK_ALLOCATED - determines if the frame list is stack allocated.
 * This prevents the heap de-allocation of the list itself.
 */
void http2FrameListDeallocate(Http2FrameListT *http2FrameList, uint8_t settings);

/**
 * Allocates a frame list and puts the http2Frame as the first element.
 * @param http2FrameList Frame list pointer to set to the newly heap allocated list.
 * @param http2Frame Frame which to put as the first element of the http2FrameList.
 * @return If all actions are successful returns EXIT_SUCCESS, otherwise ENOMEM.
 */
Http2FrameListReturnCodeT http2FrameListAllocate(Http2FrameListT **http2FrameList,
                                                 Http2FrameT *http2Frame);

/**
 * Appends the http2Frame to the http2FrameList.
 * @param http2FrameList Frame list to which to allocate the frame.
 * @param http2Frame Frame which to append to the http2FrameList.
 * @return If all actions are successful returns EXIT_SUCCESS, otherwise ENOMEM.
 */
Http2FrameListReturnCodeT http2FrameListAppend(Http2FrameListT *http2FrameList,
                                               Http2FrameT *http2Frame);

/**
 * Allocates a frame list with no elements.
 * @param http2FrameList Frame list pointer to set to the newly heap allocated list.
 * @return If all actions are successful returns EXIT_SUCCESS, otherwise ENOMEM.
 */
Http2FrameListReturnCodeT http2FrameListAllocateEmpty(Http2FrameListT **http2FrameList);

/**
 * Joins the two lists by joining the tail of the target with the head of the source
 * and deallocating the source list.
 * @param target List to which to append the elements of the source list.
 * @param source List from which to take the elements and deallocate.
 */
void http2FrameListJoin(Http2FrameListT *target, Http2FrameListT *source);

/**
 * Allocates a frame list on the heap and creates elements from the buffer,
 * @param buffer Buffer from which to create the http2FrameList.
 * @param isClient Determines if the buffer is a client or a server.
 * @param filledLength Length of the buffer which has been filled with data.
 * @param http2FrameList Frame list pointer where the allocated list is put.
 * @param parsedBuffer Length of the buffer successfully parsed.
 * The value of the pointer should be initialized to 0u.
 * @return Return code of the function. Valid return codes are:
 * - HTTP2_FRAME_LIST_BUFFER_PROCESSED - Returned if the whole buffer has been processed
 * or the buffer has only empty padding.
 * The list contains at least 1 element.
 * - HTTP2_FRAME_LIST_BUFFER_DOES_NOT_HAVE_WHOLE_BODY - The buffer has a frame header
 * but the body is incomplete.
 * The list has not been allocated.
 * - HTTP2_FRAME_LIST_BUFFER_HAS_ONLY_HEADER - The buffer has a frame header but no other data.
 * The list has not been allocated.
 * - HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY - During an allocation there was not enough memory,
 * all allocations during the function call
 * have been freed and no allocation is made.
 */
Http2FrameListReturnCodeT http2FrameListFromBytes(uint8_t *buffer,
                                                  bool isClient,
                                                  size_t filledLength,
                                                  Http2FrameListT **http2FrameList,
                                                  size_t *parsedBuffer);

/**
 * Frame list is converted into a buffer to be transmitted over the wire.
 * @param buffer Buffer pointer into which to put the frames.
 * @param http2FrameList Frame list to serialize into a buffer.
 * @return Return codes:
 * - HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY - Not enough memory on the heap.
 * - HTTP2_FRAME_LIST_SUCCESS - Everything went fine the list has been serialized.
 */
Http2FrameListReturnCodeT http2FrameListToBytes(Http2BufferT **buffer,
                                                Http2FrameListT *http2FrameList);

/**
 * Removes a frame list element from the frame list. The operation is quick O(1),
 * since the list is not iterated.
 * @param element Element to remove from the list.
 * @param frameList Frame list DTO.
 * @param deallocateFrame Determines if the frame should be deallocated as well.
 */
void http2FrameListElementRemove(Http2FrameListElementT *element,
                                 Http2FrameListT *frameList,
                                 bool deallocateFrame);

void http2DataToDataFrames(Http2FrameListT **frameList,
                           uint8_t *buffer,
                           size_t bufferLength,
                           uint32_t streamId,
                           size_t maxFrameSize);

#endif /* NS_HTTP2_LIBS_FRAME_LIST_H */
