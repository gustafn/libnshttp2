/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NS_HTTP2_LIBS_FRAME_PAYLOAD_H
#define NS_HTTP2_LIBS_FRAME_PAYLOAD_H

#include <stdlib.h>
#include <nshttp2/return_code.h>
#include <nshttp2/utility/memory.h>
#include <nshttp2/stddef.h>

/**
 * Settings identifiers as defined in https://tools.ietf.org/html/rfc7540#section-6.5.2
 */
#define SETTINGS_HEADER_TABLE_SIZE_IDENTIFIER 0x1u
#define SETTINGS_ENABLE_PUSH_IDENTIFIER 0x2u
#define SETTINGS_MAX_CONCURRENT_STREAMS_IDENTIFIER 0x3u
#define SETTINGS_INITIAL_WINDOW_SIZE_IDENTIFIER 0x4u
#define SETTINGS_MAX_FRAME_SIZE_IDENTIFIER 0x5u
#define SETTINGS_MAX_HEADER_LIST_SIZE_IDENTIFIER 0x6u

/**
 * Standard settings headers as defined in https://tools.ietf.org/html/rfc7540#section-6.5.2
 */

#define ASSUMED_SETTING_TABLE_SIZE 4096u
#define ASSUMED_SETTING_ENABLE_PUSH 1u
#define ASSUMED_SETTING_MAX_CONCURRENT_STREAMS UINT16_MAX
#define ASSUMED_SETTING_INITIAL_WINDOW_SIZE 65535u
#define ASSUMED_SETTING_MAX_FRAME_SIZE 16384u
#define ASSUMED_SETTING_MAX_HEADER_LIST_SIZE UINT16_MAX

#define DEFAULT_SETTING_HEADER_TABLE_SIZE 8192u
#define DEFAULT_SETTING_ENABLE_PUSH 1u
/* We do this to somewhat limit concurrency */
#define DEFAULT_SETTING_MAX_CONCURRENT_STREAMS 128u
#define DEFAULT_SETTING_INITIAL_WINDOW_SIZE 1048576u
#define DEFAULT_SETTING_MAX_FRAME_SIZE 65536u
#define DEFAULT_SETTING_MAX_HEADER_LIST_SIZE 163840u

#define HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE (sizeof(uint32_t) + sizeof(uint8_t))
#define HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE sizeof(uint32_t)
#define HTTP2_SETTINGS_FORMAT_SIZE (sizeof(uint16_t) + sizeof(uint32_t))
#define HTTP2_PING_FRAME_PAYLOAD_SIZE (sizeof(uint8_t) * 8)
#define HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN (sizeof(uint32_t) * 2)
#define HTTP2_WINDOW_UPDATE_PAYLOAD_LEN sizeof(uint32_t)

/**
 * HTTP/2 frame padding structure, defines the padding used in a frame.
 */
typedef struct Http2FramePadding {
    uint8_t *padding;
    uint8_t length;
} Http2FramePaddingT;

/**
 * Helper structure used to specify the buffer pointer after padding length and
 * Frame length excluding the padding at the end of the frame.
 */
typedef struct Http2BufferWithoutPadding {
    uint8_t *buffer;
    uint32_t frameLengthWithoutPadding;
} Http2BufferWithoutPaddingT;

/**
 * Helper structure used to represent the flags in frames, which use priority, as well as PRIORITY_FRAME_TYPE itself.
 */
typedef struct Http2FramePriority {
    /**
     * Indicates which stream this stream is dependent on, if any. Only present if the PRIORITY
     * flag is set.
     */
    uint32_t dependency;
    /**
     * Indicates whether the stream dependency is exclusive. Only present if the PRIORITY flag is set.
     */
    bool exclusive;
    /**
     * Indicates the relative priority of the stream. Only present if the PRIORITY flag is set.
     */
    uint8_t weight;
} Http2FramePriorityT;

/**
 * PRIORITY_FRAME_TYPE payload.
 * https://tools.ietf.org/html/rfc7540#section-6.3
 */
typedef Http2FramePriorityT Http2PriorityFramePayloadT;

/**
 * HEADERS_FRAME_TYPE payload.
 * https://tools.ietf.org/html/rfc7540#section-6.2
 */
typedef struct Http2HeadersFramePayload {
    Http2FramePriorityT priority;
    uint8_t *headerPayload;
} Http2HeadersFramePayloadT;

/**
 * RST_STREAM_FRAME_TYPE payload.
 * https://tools.ietf.org/html/rfc7540#section-6.4
 */
typedef struct Http2RstStreamFramePayload {
    uint32_t errorCode;
} Http2RstStreamFramePayloadT;

/**
 * SETTINGS_FRAME_TYPE payload.
 * https://tools.ietf.org/html/rfc7540#section-6.5
 */
typedef struct Http2SettingsFramePayload {
    uint32_t headerTableSize;
    uint32_t maxConcurrentStreams;
    uint32_t initialWindowSize;
    uint32_t maxFrameSize;
    uint32_t maxHeaderListSize;
    bool enablePush;
} Http2SettingsFramePayloadT;

/**
 * PUSH_PROMISE_FRAME_TYPE payload.
 * https://tools.ietf.org/html/rfc7540#section-6.6
 */
typedef struct Http2PushPromiseFramePayload {
    uint8_t *headerBlockFragment;
    uint32_t promisedStreamId;
} Http2PushPromiseFramePayloadT;

/**
 * PING_FRAME_TYPE payload.
 * https://tools.ietf.org/html/rfc7540#section-6.7
 */
typedef struct Http2PingFramePayload {
    uint8_t opaqueData[8];
} Http2PingFramePayloadT;

/**
 * GOAWAY_FRAME_TYPE payload.
 * https://tools.ietf.org/html/rfc7540#section-6.8
 */
typedef struct Http2GoawayFramePayload {
    uint32_t lastStreamId;
    uint32_t errorCode;
    uint8_t *additionalDebugData;
} Http2GoawayFramePayloadT;

/**
 * WINDOW_UPDATE_FRAME_TYPE payload.
 * https://tools.ietf.org/html/rfc7540#section-6.9
 */
typedef struct Http2WindowUpdatePayload {
    uint32_t windowSizeIncrement;
} Http2WindowUpdatePayloadT;

/**
 * CONTINUATION_FRAME_TYPE payload.
 * https://tools.ietf.org/html/rfc7540#section-6.10
 */
typedef struct Http2ContinuationPayload {
    uint8_t *headerBlockFragment;
} Http2ContinuationPayloadT;

/**
 * Fills the provided buffer of length HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE
 * with the data in http2PriorityFramePayload.
 * @param http2PriorityFramePayload Priority frame DTO.
 * @param buffer Buffer to fill with the priority frame.
 */
void http2PriorityFramePayloadToBytes(Http2PriorityFramePayloadT *http2PriorityFramePayload,
                                      uint8_t *buffer);

/**
 * Decodes the buffer of length HTTP2_PRIORITY_FRAME_PAYLOAD_SIZE
 * into http2PriorityFramePayload.
 * @param http2PriorityFramePayload Priority frame DTO.
 * @param buffer Buffer from which to read the priority frame.
 */
void http2PriorityFramePayloadFromBytes(Http2PriorityFramePayloadT *http2PriorityFramePayload,
                                        const uint8_t *buffer);

/**
 * Fills the provided buffer of length frameLength - HTTP2_FRAME_HEADER_SIZE
 * with the data in http2DataFramePayload.
 * @param http2DataFramePayload Data frame DTO.
 * @param buffer Buffer to fill with the priority frame.
 * @param frameLength Length of the frame, this includes the HTTP2_FRAME_HEADER_SIZE + the size of the payload.
 */
void http2DataFramePayloadToBytes(uint8_t *http2DataFramePayload,
                                  uint8_t *buffer,
                                  uint32_t frameLength,
                                  Http2FramePaddingT *http2FramePadding);

/**
 * Decodes the buffer of length frameLength - HTTP2_FRAME_HEADER_SIZE
 * into http2DataFramePayload;
 * @param http2DataFramePayload Data frame DTO.
 * @param buffer Buffer from which to read the data frame.
 * @param frameLength Length of the frame, this includes the HTTP2_FRAME_HEADER_SIZE + the size of the payload.
 * @param hasPadding Specifies if the buffer has padding or not.
 */
void http2DataFramePayloadFromBytes(uint8_t *http2DataFramePayload,
                                    uint32_t *frameLengthWithoutPadding,
                                    uint8_t *buffer,
                                    uint32_t frameLength,
                                    bool hasPadding);

/**
 * Fills the provided buffer of length frameLength - HTTP2_FRAME_HEADER_SIZE
 * with the data in http2HeadersFramePayload.
 * @param http2HeadersFramePayload Headers frame DTO.
 * @param buffer Buffer to fill with the headers frame.
 * @param frameLength Length of the frame, this includes the HTTP2_FRAME_HEADER_SIZE + the size of the payload.
 */
void http2HeadersFramePayloadToBytes(Http2HeadersFramePayloadT *http2HeadersFramePayload,
                                     uint8_t *buffer,
                                     uint32_t frameLength,
                                     Http2FramePaddingT *http2FramePadding,
                                     bool hasPriority);

/**
 * Decodes the buffer of length frameLength - HTTP2_FRAME_HEADER_SIZE
 * into http2HeadersFramePayload.
 * @param http2HeadersFramePayload Headers frame DTO.
 * @param buffer Buffer from which to read the data frame.
 * @param frameLength Length of the frame, this includes the HTTP2_FRAME_HEADER_SIZE + the size of the payload.
 */
void http2HeadersFramePayloadFromBytes(Http2HeadersFramePayloadT *http2HeadersFramePayload,
                                       uint32_t *frameLengthWithoutPadding,
                                       uint8_t *buffer,
                                       uint32_t frameLength,
                                       bool hasPadding,
                                       bool hasPriority);

/**
 * Fills the provided buffer of the length HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE
 * with the data in http2RstStreamFramePayload.
 * @param http2RstStreamFramePayload RST stream frame DTO.
 * @param buffer Buffer to fill with the RST stream frame.
 */
void http2RstStreamFramePayloadToBytes(Http2RstStreamFramePayloadT *http2RstStreamFramePayload,
                                       uint8_t *buffer);

/**
 * Decodes the buffer of length HTTP2_RST_STREAM_FRAME_PAYLOAD_SIZE
 * into http2RstStreamFramePayload.
 * @param http2RstStreamFramePayload RST stream frame DTO.
 * @param buffer Buffer from which to read the RST stream frame.
 */
void http2RstStreamFramePayloadFromBytes(Http2RstStreamFramePayloadT *http2RstStreamFramePayload,
                                         const uint8_t *buffer);

/**
 * Fills to provided buffer of length HTTP2_SETTINGS_FORMAT_SIZE * the number of non-default parameters
 * with the data in http2_settings_frame_payload.
 * @param http2_settings_frame_payload Settings frame DTO.
 * @param buffer Buffer to fill with the Settings frame.
 */
void http2SettingsFramePayloadToBytes(Http2SettingsFramePayloadT *http2_settings_frame_payload,
                                      uint8_t *buffer);

/**
 * Decodes the buffer of length HTTP2_SETTINGS_FORMAT_SIZE * the number of settings in the buffer.
 * @param http2SettingsFramePayload Settings frame DTO.
 * @param buffer Buffer from which to decode the settings frame.
 * @param frameLength Length of the frame, this includes the HTTP2_FRAME_HEADER_SIZE + the size of the payload.
 * @param isClient Determines if the Settings frame is meant to be sent as a HTTP/2 client.
 */
void http2SettingsFramePayloadFromBytes(Http2SettingsFramePayloadT *http2SettingsFramePayload,
                                        uint8_t *buffer,
                                        uint32_t frameLength,
                                        bool isClient);

/**
 * Fills the provided buffer with the push promise payload.
 */
void http2PushPromiseFramePayloadToBytes(Http2PushPromiseFramePayloadT *http2PushPromiseFramePayload,
                                         uint8_t *buffer,
                                         uint32_t frameLength,
                                         Http2FramePaddingT *http2FramePadding);

void http2PushPromiseFramePayloadFromBytes(Http2PushPromiseFramePayloadT *http2PushPromiseFramePayload,
                                           uint8_t *buffer,
                                           uint32_t frameLength,
                                           bool hasPadding);

void http2PingFramePayloadToBytes(Http2PingFramePayloadT *http2PingFramePayload,
                                  uint8_t *buffer);

void http2PingFramePayloadFromBytes(Http2PingFramePayloadT *http2PingFramePayload,
                                    uint8_t *buffer);

void http2GoawayFramePayloadToBytes(Http2GoawayFramePayloadT *http2GoawayFramePayload,
                                    uint8_t *buffer,
                                    uint32_t payloadLength);

void http2GoawayFramePayloadFromBytes(Http2GoawayFramePayloadT *http2GoawayFramePayload,
                                      const uint8_t *buffer,
                                      uint32_t payloadLength);

void http2WindowUpdatePayloadToBytes(Http2WindowUpdatePayloadT *http2WindowUpdatePayload,
                                     uint8_t *buffer);

void http2WindowUpdatePayloadFromBytes(Http2WindowUpdatePayloadT *http2WindowUpdatePayload,
                                       const uint8_t *buffer);

void http2ContinuationPayloadToBytes(Http2ContinuationPayloadT *http2ContinuationPayload,
                                     uint8_t *buffer,
                                     uint32_t frameLength);

void http2ContinuationPayloadFromBytes(Http2ContinuationPayloadT *http2ContinuationPayload,
                                       uint8_t *buffer,
                                       uint32_t frameLength);

Http2ReturnCodeT
http2PriorityFramePayloadAllocate(Http2PriorityFramePayloadT **http2PriorityFramePayload);

void http2PriorityFramePayloadDeallocate(Http2PriorityFramePayloadT *http2PriorityFramePayload);

Http2ReturnCodeT http2HeadersFramePayloadAllocate(Http2HeadersFramePayloadT **http2HeadersFramePayload,
                                                  bool hasPriority,
                                                  uint32_t frameLength);

void http2HeadersFramePayloadDeallocate(Http2HeadersFramePayloadT *http2HeadersFramePayload);

Http2ReturnCodeT
http2RstStreamFramePayloadAllocate(Http2RstStreamFramePayloadT **http2RstStreamFramePayload);

void http2RstStreamFramePayloadDeallocate(Http2RstStreamFramePayloadT *http2RstStreamFramePayload);

Http2ReturnCodeT http2SettingsFramePayloadAllocate(Http2SettingsFramePayloadT **http2SettingsFramePayload);

void http2SettingsFramePayloadDeallocate(Http2SettingsFramePayloadT *http2SettingsFramePayload);

Http2ReturnCodeT
http2PushPromiseFramePayloadAllocate(Http2PushPromiseFramePayloadT **http2PushPromiseFramePayload,
                                     uint32_t frameLength);

void http2PushPromiseFramePayloadDeallocate(Http2PushPromiseFramePayloadT *http2PushPromiseFramePayload);

Http2ReturnCodeT http2PingFramePayloadAllocate(Http2PingFramePayloadT **http2PingFramePayload);

void http2PingFramePayloadDeallocate(Http2PingFramePayloadT *http2PingFramePayload);

Http2ReturnCodeT http2GoawayFramePayloadAllocate(Http2GoawayFramePayloadT **http2GoawayFramePayload,
                                                 size_t frameLength);

void http2GoawayFramePayloadDeallocate(Http2GoawayFramePayloadT *http2GoawayFramePayload);

Http2ReturnCodeT http2WindowUpdatePayloadAllocate(Http2WindowUpdatePayloadT **http2WindowUpdatePayload);

void http2WindowUpdatePayloadDeallocate(Http2WindowUpdatePayloadT *http2WindowUpdatePayload);

Http2ReturnCodeT http2ContinuationPayloadAllocate(Http2ContinuationPayloadT **http2ContinuationPayload,
                                                  uint32_t frameLength);

void http2ContinuationPayloadDeallocate(Http2ContinuationPayloadT *http2ContinuationPayload);

/**
 * Returns a structure containing the starting point of the buffer without the padding length,
 * as well as the size of the frame without the padding at end and the padding length at the start.
 * @param buffer Buffer pointer used to determine the new buffer pointer.
 * @param frameLength Frame length found in the frame header.
 */
void http2BufferRemovePadding(uint8_t *buffer,
                              uint32_t frameLength,
                              Http2BufferWithoutPaddingT *bufferWithoutPadding);

/**
 * Create a frame payload with default frame settings. Default settings are those found in the macros:
 * DEFAULT_SETTING_TABLE_SIZE
 * DEFAULT_SETTING_ENABLE_PUSH
 * DEFAULT_SETTING_MAX_CONCURRENT_STREAMS
 * DEFAULT_SETTING_INITIAL_WINDOW_SIZE
 * DEFAULT_SETTING_MAX_FRAME_SIZE
 * DEFAULT_SETTING_MAX_HEADER_LIST_SIZE
 * @param settingsFramePayload Frame payload pointer to put the settings in.
 */
void http2FramePayloadInitDefaultSettings(Http2SettingsFramePayloadT *settingsFramePayload);

#endif /* NS_HTTP2_LIBS_FRAME_PAYLOAD_H */
