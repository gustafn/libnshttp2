/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef NS_HTTP2_LIBS_FRAME_H
#define NS_HTTP2_LIBS_FRAME_H

#include <nshttp2/frame_payload.h>
#include <nshttp2/utility/memory.h>
#include <nshttp2/utility/string_utils.h>
#include <stdlib.h>
#include <nshttp2/stddef.h>

/* It is required for clients to send this preface first when establishing a connection */
#define HTTP2_CONNECTION_PREFACE "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"
#define HTTP2_CONNECTION_PREFACE_LEN 24u

/* Frame header length */
#define HTTP2_FRAME_HEADER_SIZE 9u

/* Minimum frame length (payload) */
#define HTTP2_FRAME_LENGTH_MIN 16384u
/* Maximum frame length (payload) */
#define HTTP2_FRAME_LENGTH_MAX 16777215u

/* Exclusive bit mask */
#define HTTP2_EXCLUSIVE_FLAG_BITS 0x80u

/* Frame types */
#define DATA_FRAME_TYPE 0x0u
#define HEADERS_FRAME_TYPE 0x1u
#define PRIORITY_FRAME_TYPE 0x2u
#define RST_STREAM_FRAME_TYPE 0x3u
#define SETTINGS_FRAME_TYPE 0x4u
#define PUSH_PROMISE_FRAME_TYPE 0x5u
#define PING_FRAME_TYPE 0x6u
#define GOAWAY_FRAME_TYPE 0x7u
#define WINDOW_UPDATE_FRAME_TYPE 0x8u
#define CONTINUATION_FRAME_TYPE 0x9u
/* https://tools.ietf.org/html/rfc7838 */
#define ALTSVC_FRAME_TYPE 0xau

/* Frame type string representations */
#define DATA_FRAME_NAME "data"
#define HEADERS_FRAME_NAME "headers"
#define PRIORITY_FRAME_NAME "priority"
#define RST_STREAM_FRAME_NAME "rst stream"
#define SETTINGS_FRAME_NAME "settings"
#define PUSH_PROMISE_FRAME_NAME "push promise"
#define PING_FRAME_NAME "ping"
#define GOAWAY_FRAME_NAME "goaway"
#define WINDOW_UPDATE_FRAME_NAME "window"
#define CONTINUATION_FRAME_NAME "continuation"

/* Proposed frame types */
/* https://tools.ietf.org/html/draft-ietf-httpbis-origin-frame-06 */
#define ORIGIN_FRAME_TYPE 0xcu
/* https://tools.ietf.org/html/draft-ietf-httpbis-cache-digest-05 */
#define CACHE_DIGEST_FRAME_TYPE 0xdu

/* Error codes for HTTP2 connection */
#define NO_ERROR_ERROR_CODE 0x0u
#define PROTOCOL_ERROR_ERROR_CODE 0x1u
#define INTERNAL_ERROR_ERROR_CODE 0x2u
#define FLOW_CONTROL_ERROR_ERROR_CODE 0x3u
#define SETTINGS_TIMEOUT_ERROR_CODE 0x4u
#define STREAM_CLOSED_ERROR_CODE 0x5u
#define FRAME_SIZE_ERROR_ERROR_CODE 0x6u
#define REFUSED_STREAM_ERROR_CODE 0x7u
#define CANCEL_ERROR_CODE 0x8u
#define COMPRESSION_ERROR_ERROR_CODE 0x9u
#define CONNECT_ERROR_ERROR_CODE 0xa
#define ENHANCE_YOUR_CALM_ERROR_CODE 0xb
#define INADEQUATE_SECURITY_ERROR_CODE 0xc
#define HTTP_1_1_REQUIRED_ERROR_CODE 0xd

/* General frame flags */
#define DEFAULT_FRAME_FLAG 0x0u
#define END_STREAM_FRAME_FLAG 0x1u
#define END_HEADERS_FRAME_FLAG 0x4u
#define PADDED_FRAME_FLAG 0x8u
#define PRIORITY_FRAME_FLAG 0x20u
#define ACK_FRAME_FLAG 0x1u

typedef enum Http2FrameReturnCode {
    UNSUPPORTED_FRAME_TYPE,
    FRAME_SUCCESS,
    FRAME_NO_MEMORY
} Http2FrameReturnCodeT;

/**
 * When an endpoint receives a PING frame
 * without an ACK sent, it must send a PING frame back with the ACK flag set and the
 * same opaque data.
 */
#define ACK_FRAME_FLAG 0x1u

typedef struct Http2FrameHeader {
    /**
     * Length:  The length of the frame payload expressed as an unsigned
     * 24-bit integer. Values greater than 2^14 (16,384) MUST NOT be
     * sent unless the receiver has set a larger value for
     * SETTINGS_MAX_FRAME_SIZE.
     */
    uint32_t length;
    /**
     * Type:  The 8-bit type of the frame. The frame type determines the
     * format and semantics of the frame. Implementations MUST ignore
     * and discard any frame that has a type that is unknown.
      */
    uint8_t type;
    /**
     * Flags:  An 8-bit field reserved for boolean flags specific to the
     * frame type.
     *
     * Flags are assigned semantics specific to the indicated frame type.
     * Flags that have no defined semantics for a particular frame type
     * MUST be ignored and MUST be left unset (0x0) when sending.
     */
    uint8_t flags;
    /**
     * A Stream ID is made up of R and the stream identifier.
     * R: A reserved 1-bit field. The semantics of this bit are undefined,
     * and the bit MUST remain unset (0x0) when sending and MUST be
     * ignored when receiving.
     *
     * Stream Identifier: A stream identifier expressed
     * as an unsigned 31-bit integer. The value 0x0 is reserved for
     * frames that are associated with the connection as a whole as
     * opposed to an individual stream.
     */
    uint32_t streamId;
} Http2FrameHeaderT;

/**
 * Union type of different payloads supported for frames based on the frame type.
 */
typedef union Http2FramePayload {
    uint8_t *http2DataFramePayload;
    Http2HeadersFramePayloadT *http2HeadersFramePayload;
    Http2PriorityFramePayloadT *http2PriorityFramePayload;
    Http2RstStreamFramePayloadT *http2RstStreamFramePayload;
    Http2SettingsFramePayloadT *http2SettingsFramePayload;
    Http2PushPromiseFramePayloadT *http2PushPromiseFramePayload;
    Http2PingFramePayloadT *http2PingFramePayload;
    Http2GoawayFramePayloadT *http2GoawayFramePayload;
    Http2WindowUpdatePayloadT *http2WindowUpdatePayload;
    Http2ContinuationPayloadT *http2ContinuationPayload;
} Http2FramePayloadT;

/**
 * Represents a general HTTP/2 data structure.
 */
typedef struct Http2Frame {
    /**
     * Frame header, same for each type of frame.
     */
    Http2FrameHeaderT header;
    /**
      Frame data in octets. The size of a frame payload is limited by the maximum size that a
      receiver advertises in the SETTINGS_MAX_FRAME_SIZE.
     */
    Http2FramePayloadT payload;
} Http2FrameT;

/**
 * Checks if the buffer starts with a buffer where the frame header is not completely empty.
 * This does not check if the frame header itself is valid.
 * @param buffer Buffer to check, has to be at least HTTP2_FRAME_HEADER_SIZE long.
 * @return Boolean indicating if the buffer starts with a completely empty buffer.
 */
bool http2IsNonZeroHeaderBuffer(uint8_t *buffer);

/**
 * Serializes and writes the frame header into the buffer of length HTTP2_FRAME_HEADER_SIZE.
 * @param http2FrameHeader Frame header DTO.
 * @param buffer Buffer into which to write the serialized frame header.
 */
void http2FrameHeaderToBytes(const Http2FrameHeaderT *http2FrameHeader,
                             uint8_t *buffer);

/**
 * Deserializes the frame header into http2FrameHeader. Please call only once
 * you have verified the buffer with http2IsNonZeroHeaderBuffer or you are sure
 * that the buffer contains a valid frame header.
 * @param http2FrameHeader Frame header DTO to fill with the data.
 * @param buffer Buffer from which to deserialize the frame header.
 */
void http2FrameHeaderFromBytes(Http2FrameHeaderT *http2FrameHeader,
                               uint8_t *buffer);

/**
 * Deserializes the frame payload into the http2Frame payload field,
 * assumes that the http2Frame has the header already filled.
 * @param http2Frame Frame DTO to deserialize into.
 * @param isClient Boolean indicating if the frame is coming from a client.
 * @param buffer Buffer from which to deserialize the frame payload.
 * @return Return codes:
 * - HTTP2_SUCCESS - Everything went fine.
 * - HTTP2_UNSUPPORTED_FRAME_TYPE - Provided frame type is unsupported.
 */
Http2ReturnCodeT http2FrameFromBytesHeaderSet(Http2FrameT *http2Frame,
                                              bool isClient,
                                              uint8_t *buffer);

/**
 * Deserializes the frame from the buffer. You must make sure that the payload
 * is allocated and has the exact right size. You can get a heap allocated correct
 * frame using http2FramePayloadAllocate.
 * has the exact right size beforehand.
 * @param http2Frame Frame DTO to deserialize into.
 * @param isClient Boolean indicating if the frame is coming from a client.
 * @param buffer Buffer from which to deserialize the frame.
 * @return Return codes:
 * - HTTP2_SUCCESS - Everything went fine.
 * - HTTP2_UNSUPPORTED_FRAME_TYPE - Provided frame type is unsupported.
 */
Http2ReturnCodeT http2FrameFromBytes(Http2FrameT *http2Frame,
                                     bool isClient,
                                     uint8_t *buffer);

/**
 * Serializes the frame to the buffer.
 * @param http2Frame Frame to serialize.
 * @param buffer Buffer into which to serialize the frame.
 * @param http2FramePadding Padding to put in the frame.
 * For no padding specify NULL.
 * @return Returns:
 * NO_ERROR_ERROR_CODE if the frame header has a valid type
 * UNSUPPORTED_FRAME_TYPE for unsupported frame type.
 */
Http2FrameReturnCodeT http2FrameToBytes(const Http2FrameT *http2Frame,
                                        uint8_t *buffer,
                                        Http2FramePaddingT *http2FramePadding);

/**
 * Allocates a buffer and fills it with the frame.
 * @param http2Frame Frame to serialize.
 * @param buffer Buffer into which to serialize the frame.
 * @param http2FramePadding Padding to put in the frame.
 * For no padding specify NULL.
 * @return Returns:
 * NO_ERROR_ERROR_CODE if the frame header has a valid type.
 * UNSUPPORTED_FRAME_TYPE for unsupported frame type.
 * HTTP2_NO_MEMORY not enough memory to allocate the buffer.
 */
Http2FrameReturnCodeT http2FrameToBytesWithAllocation(Http2FrameT *http2Frame,
                                                      uint8_t **buffer,
                                                      size_t *bufferSize,
                                                      Http2FramePaddingT *http2FramePadding);

/**
 * Checks if the buffer starts with the HTTP2_CONNECTION_PREFACE string.
 * @param buffer Buffer which to check.
 * @return Boolean indicating if the buffer starts with HTTP2_CONNECTION_PREFACE.
 */
bool http2IsValidConnectionPreface(uint8_t *buffer);

/**
 * Allocates the frame payload on the heap, based on the type and frame length.
 * @param http2FramePayload Frame payload allocated.
 * @param type Type of frame.
 * @param flags Flags for the frame.
 * @param frameLength Frame length.
 * @return Returns the following codes:
 * HTTP2_NO_MEMORY - There was not enough memory for the allocation.
 * HTTP2_UNSUPPORTED_FRAME_TYPE - Unsupported frame type.
 * HTTP2_NO_ERROR_ERROR_CODE - Everything went fine.
 */
Http2ReturnCodeT http2FramePayloadAllocate(Http2FramePayloadT *http2FramePayload,
                                           uint8_t flags,
                                           uint8_t type,
                                           uint32_t frameLength);

/**
 * Deallocates the frame payload, based on the type of the frame.
 * @param http2FramePayload Frame payload to deallocate.
 * @param type Type of frame.
 */
void http2FramePayloadDeallocate(Http2FramePayloadT *http2FramePayload,
                                 uint8_t type);

/**
 * Allocates frame on the heap, based on the type and length.
 * @param http2Frame Frame allocated.
 * @param type Type of frame.
 * @param flags Flags for the frame.
 * @param frameLength Frame length.
 * @return Return codes:
 * - HTTP2_NO_MEMORY if there was not enough memory for the allocation.
 * - HTTP2_SUCCESS if everything went fine.
 * - HTTP2_UNSUPPORTED_FRAME_TYPE the provided frame is not supported.
 */
Http2ReturnCodeT http2FrameAllocate(Http2FrameT **http2Frame,
                                    uint8_t type,
                                    uint8_t flags,
                                    uint32_t frameLength);

/**
 * Deallocates the frame.
 * @param http2Frame Frame deallocated.
 */
void http2FrameDeallocate(Http2FrameT *http2Frame);


/**
 * Retrieves the name of the frame type in string format.
 * @param frameTypeId Frame type id.
 * @return String representation of the frame type.
 */
char *http2FrameTypeName(uint8_t frameTypeId);

/**
 * Determines if the frame is a control frame.
 * @param http2Frame Frame to check.
 * @return Boolean indicating if the frame is a control frame.
 */
bool http2FrameIsStreamFrame(Http2FrameT *http2Frame);

/**
 * Determines if the frame is a non-control frame.
 * @param http2Frame Frame to check.
 * @return Boolean indicating if the frame is a control frame.
 */
bool http2FrameIsNonControlFrame(Http2FrameT *http2Frame);

/**
 * Returns if the frame has the priority flag set.
 * @param http2Frame HTTP/2 frame DTO.
 * @return Boolean indicating if the frame has a priority flag.
 */
bool http2FrameHasPriority(Http2FrameT *http2Frame);

/**
 * Returns if the frame is the last header. The frame has to be a header or continuation frame.
 * @param http2Frame HTTP/2 frame DTO.
 * @return Boolean indicating if the frame is the last header frame.
 */
bool http2FrameIsLastHeader(Http2FrameT *http2Frame);

/**
 * Determines the required frame buffer size.
 * @param http2Frame HTTP/2 frame DTO.
 * @return Size of the required buffer for the frame.
 */
uint32_t http2FrameGetRequiredFrameSize(Http2FrameT *http2Frame);

/**
 * Returns boolean indicating if the frame is supported by the library.
 * @param http2Frame HTTP
 * @return Boolean indicating if the frame is supported.
 */
bool http2FrameIsSupported(Http2FrameT *http2Frame);

#endif /* NS_HTTP2_LIBS_FRAME_H */
