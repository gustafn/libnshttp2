/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/stream_hashmap.h>
#include <nshttp2/stream.h>

Http2ReturnCodeT streamHashmapAllocate(StreamHashmapT **streamHashmap) {
    *streamHashmap = http2Malloc(sizeof(StreamHashmapT));

    if (*streamHashmap == NULL) {
        return HTTP2_NO_MEMORY;
    }

    (*streamHashmap)->hashmap = kh_init_hashmap();

    if ((*streamHashmap)->hashmap == NULL) {
        http2Free(*streamHashmap);
        return HTTP2_NO_MEMORY;
    }

    switch (pthread_mutex_init(&(*streamHashmap)->mutex, NULL)) {
        case 0:
            return HTTP2_SUCCESS;
        default:
            kh_destroy_hashmap((*streamHashmap)->hashmap);
            http2Free(*streamHashmap);
            return HTTP2_INTERNAL_ERROR;
    }
}

void streamHashmapInsert(StreamHashmapT *const streamHashmap,
                         struct Http2Stream *const stream) {
    int absent;
    khint_t location;

    location = kh_put_hashmap(streamHashmap->hashmap, stream->streamId, &absent);
    kh_value(streamHashmap->hashmap, location) = stream;
}

void streamHashmapInsertWithStreamId(StreamHashmapT *const streamHashmap,
                                     const uint32_t streamId,
                                     Http2StreamT *const stream) {
    int absent;
    khint_t location;

    location = kh_put_hashmap(streamHashmap->hashmap, streamId, &absent);
    kh_value(streamHashmap->hashmap, location) = stream;
}

void streamHashmapRemove(StreamHashmapT *const streamHashmap,
                         const uint_fast32_t streamId) {
    khint_t location;

    location = kh_get_hashmap(streamHashmap->hashmap, streamId);

    if (location == kh_end(streamHashmap->hashmap)) {
        return;
    }

    if (kh_exist(streamHashmap->hashmap, location)) {
        kh_del_hashmap(streamHashmap->hashmap, location);
    }
}

void streamHashmapGet(StreamHashmapT *const streamHashmap,
                      const uint_fast32_t streamId,
                      Http2StreamT **stream) {
    kh_hashmap_t *hashmap;
    khint_t location;
    *stream = NULL;

    if (streamHashmap == NULL) {
        return;
    }

    hashmap = streamHashmap->hashmap;

    if (hashmap == NULL || (kh_size(hashmap) == 0u)) {
        return;
    }

    location = kh_get_hashmap(hashmap, streamId);

    if (location == kh_end(hashmap)) {
        return;
    }

    if (kh_exist(hashmap, location)) {
        *stream = kh_value(hashmap, location);
    }
}

void streamHashmapDeallocate(StreamHashmapT *const streamHashmap) {
    /* Destroy the mutex */
    pthread_mutex_destroy(&streamHashmap->mutex);
    /* Free the kh hashmap */
    kh_destroy_hashmap(streamHashmap->hashmap);
    /* Freeing the container */
    http2Free(streamHashmap);
}
