/*
MIT License
-----------

Copyright (c) 2021 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/buffer/temporary_buffer.h>
#include <nshttp2/utility/memory.h>
#include <string.h>
#include <nshttp2/utility/string_utils.h>

#ifdef __unix__

#include <unistd.h>
#include <sys/mman.h>

#else
/* TODO implement memory mapped files for Windows */

#endif

Http2ReturnCodeT http2TemporaryBufferAllocate(Http2TemporaryBufferT **http2TemporaryBuffer,
                                              const size_t maxIncomingDataInMemory) {
    Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBufferPtr =
            http2Calloc(sizeof(Http2TemporaryBufferT));

    if (http2TemporaryBufferPtr == NULL) {
        return HTTP2_NO_MEMORY;
    }

    http2TemporaryBufferPtr->maxIncomingDataInMemory = maxIncomingDataInMemory;
    *http2TemporaryBuffer = http2TemporaryBufferPtr;
    return HTTP2_SUCCESS;
}

void http2TemporaryBufferDeallocate(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer) {
#ifdef __unix__
    if (http2TemporaryBuffer->mappedBuffer != NULL) {
        http2TemporaryBufferCloseData(http2TemporaryBuffer);
        close(http2TemporaryBuffer->temporaryFileDescriptor);
        http2Free(http2TemporaryBuffer->temporaryFile);
    } else if (http2TemporaryBuffer->memoryBuffer != NULL) {
        http2Free(http2TemporaryBuffer->memoryBuffer);
    }
#else
    if (http2TemporaryBuffer->memoryBuffer != NULL) {
        http2Free(http2TemporaryBuffer->memoryBuffer);
    }
#endif

    http2Free(http2TemporaryBuffer);
}

static Http2ReturnCodeT
http2TemporaryBufferMemoryAppend(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer,
                                 uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                 const size_t bufferLength) {
    if (http2TemporaryBuffer->memoryBuffer == NULL) {
        http2TemporaryBuffer->memoryBuffer = http2Malloc(bufferLength);

        if (http2TemporaryBuffer->memoryBuffer == NULL) {
            return HTTP2_NO_MEMORY;
        }

        memcpy(http2TemporaryBuffer->memoryBuffer, buffer, bufferLength);
        http2TemporaryBuffer->bufferLength = bufferLength;
    } else {
        uint8_t *newBuffer = http2Realloc(http2TemporaryBuffer->memoryBuffer,
                                          http2TemporaryBuffer->bufferLength + bufferLength);

        if (newBuffer == NULL) {
            return HTTP2_NO_MEMORY;
        } else {
            http2TemporaryBuffer->memoryBuffer = newBuffer;
        }

        memcpy(http2TemporaryBuffer->memoryBuffer,
               buffer + http2TemporaryBuffer->bufferLength,
               bufferLength);

        http2TemporaryBuffer->bufferLength += bufferLength;
    }

    return HTTP2_SUCCESS;
}

#ifdef __unix__

static Http2ReturnCodeT getMemoryMappedFile(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer,
                                            uint8_t **buffer,
                                            size_t *HTTP2_CONST_RESTRICT_SPECIFIER bufferLength) {
    *buffer = mmap(NULL,
                   http2TemporaryBuffer->bufferLength,
                   PROT_READ,
                   MAP_SHARED,
                   http2TemporaryBuffer->temporaryFileDescriptor,
                   0);

    if (*buffer == NULL) {
        return HTTP2_INTERNAL_ERROR;
    }

    http2TemporaryBuffer->mappedBuffer = *buffer;

    *bufferLength = http2TemporaryBuffer->bufferLength;

    return HTTP2_SUCCESS;
}

static Http2ReturnCodeT
http2TemporaryBufferFileAppend(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer,
                               uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                               const size_t bufferLength,
                               const char *temporaryFileTemplate) {
    int temporaryFileDescriptor = http2TemporaryBuffer->temporaryFileDescriptor;

    if (temporaryFileDescriptor == 0) {
        char *temporaryFile;

        switch (stringDuplicate(temporaryFileTemplate, &temporaryFile)) {
            case HTTP2_NO_MEMORY:
                return HTTP2_NO_MEMORY;
            default:
                break;
        }

        temporaryFileDescriptor = mkstemp(temporaryFile);

        if (temporaryFileDescriptor > 0) {
            http2Free(temporaryFile);
            return HTTP2_INTERNAL_ERROR;
        } else {
            http2TemporaryBuffer->temporaryFile = temporaryFile;
            http2TemporaryBuffer->temporaryFileDescriptor = temporaryFileDescriptor;

            if (http2TemporaryBuffer->memoryBuffer != NULL) {
                ssize_t written = pwrite(temporaryFileDescriptor,
                                         http2TemporaryBuffer->memoryBuffer,
                                         http2TemporaryBuffer->bufferLength,
                                         0);

                if (written < 0 || (size_t) written != bufferLength) {
                    return HTTP2_INTERNAL_ERROR;
                }

                http2Free(http2TemporaryBuffer->memoryBuffer);
                http2TemporaryBuffer->memoryBuffer = NULL;

                written = pwrite(temporaryFileDescriptor,
                                 buffer,
                                 bufferLength,
                                 (off_t) http2TemporaryBuffer->bufferLength);

                if (written < 0 || (size_t) written != bufferLength) {
                    return HTTP2_INTERNAL_ERROR;
                }
            } else {
                const ssize_t written = pwrite(temporaryFileDescriptor,
                                               buffer,
                                               bufferLength,
                                               (__off_t) http2TemporaryBuffer->bufferLength);

                if (written < 0 || (size_t) written != bufferLength) {
                    return HTTP2_INTERNAL_ERROR;
                }
            }
        }
    } else {
        const ssize_t written = pwrite(temporaryFileDescriptor,
                                       buffer,
                                       bufferLength,
                                       (__off_t) http2TemporaryBuffer->bufferLength);

        if (written < 0 || (size_t) written != bufferLength) {
            return HTTP2_INTERNAL_ERROR;
        }
    }

    http2TemporaryBuffer->bufferLength += bufferLength;
    return HTTP2_SUCCESS;
}

#endif

#ifdef __unix__

Http2ReturnCodeT http2TemporaryBufferAppend(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer,
                                            uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                            const size_t bufferLength,
                                            const char *temporaryFileTemplate) {
    const size_t maxDataInMemory = http2TemporaryBuffer->maxIncomingDataInMemory;
    const size_t newBufferLength = http2TemporaryBuffer->bufferLength + bufferLength;

    if (temporaryFileTemplate != NULL) {
        if (newBufferLength > maxDataInMemory) {
            return http2TemporaryBufferFileAppend(http2TemporaryBuffer,
                                                  buffer,
                                                  bufferLength,
                                                  temporaryFileTemplate);
        } else {
            return http2TemporaryBufferMemoryAppend(http2TemporaryBuffer, buffer, bufferLength);
        }
    } else {
        return http2TemporaryBufferMemoryAppend(http2TemporaryBuffer, buffer, bufferLength);
    }
}

#else

Http2ReturnCodeT http2TemporaryBufferAppend(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer,
                                            uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer,
                                            const size_t bufferLength,
                                            const char *UNUSED(temporaryFileTemplate)) {
    return http2TemporaryBufferMemoryAppend(http2TemporaryBuffer, buffer, bufferLength);
}

#endif

Http2ReturnCodeT http2TemporaryBufferGetData(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer,
                                             uint8_t **buffer,
                                             size_t *HTTP2_CONST_RESTRICT_SPECIFIER bufferLength) {
#ifdef __unix__
    if (http2TemporaryBuffer->memoryBuffer != NULL) {
        *buffer = http2TemporaryBuffer->memoryBuffer;
        *bufferLength = http2TemporaryBuffer->bufferLength;
    } else {
        return getMemoryMappedFile(http2TemporaryBuffer, buffer, bufferLength);
    }
#else
    if (http2TemporaryBuffer->memoryBuffer != NULL) {
        *buffer = http2TemporaryBuffer->memoryBuffer;
        *bufferLength = http2TemporaryBuffer->bufferLength;
    }
#endif

    return HTTP2_SUCCESS;
}

#ifdef __unix__

Http2ReturnCodeT
http2TemporaryBufferCloseData(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer) {
    if (http2TemporaryBuffer->mappedBuffer != NULL) {
        int returnCode = munmap(http2TemporaryBuffer->mappedBuffer, http2TemporaryBuffer->bufferLength);

        if (returnCode != 0) {
            return HTTP2_INTERNAL_ERROR;
        }
    }

    return HTTP2_SUCCESS;
}

#else

Http2ReturnCodeT
http2TemporaryBufferCloseData(Http2TemporaryBufferT *UNUSED(http2TemporaryBuffer)) {
    return HTTP2_SUCCESS;
}

#endif

bool http2TemporaryBufferHasData(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer) {
#ifdef __unix__
    if ((http2TemporaryBuffer->memoryBuffer != NULL) || (http2TemporaryBuffer->temporaryFileDescriptor != 0)) {
        return true;
    } else {
        return false;
    }
#else
    if (http2TemporaryBuffer->memoryBuffer != NULL) {
        return true;
    } else {
        return false;
    }
#endif
}

void http2TemporaryBufferReset(Http2TemporaryBufferT *HTTP2_CONST_RESTRICT_SPECIFIER http2TemporaryBuffer) {
#ifdef __unix__
    if (http2TemporaryBuffer->mappedBuffer != NULL) {
        http2TemporaryBufferCloseData(http2TemporaryBuffer);
        close(http2TemporaryBuffer->temporaryFileDescriptor);
        http2Free(http2TemporaryBuffer->temporaryFile);
    } else if (http2TemporaryBuffer->memoryBuffer != NULL) {
        http2Free(http2TemporaryBuffer->memoryBuffer);
    }
#else
    if (http2TemporaryBuffer->memoryBuffer != NULL) {
        http2Free(http2TemporaryBuffer->memoryBuffer);
    }
#endif

    memset(http2TemporaryBuffer, 0, sizeof(Http2TemporaryBufferT));
}
