/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/utility/memory.h>
#include <nshttp2/stddef.h>
#include <nshttp2/utility/string_utils.h>
#include <string.h>

HTTP2_INLINE_SPECIFIER bool stringStartWithSubstring(const char *const str1,
                                                     const char *const str2,
                                                     const size_t len) {
    return memcmp(str1, str2, len) == 0;
}

Http2ReturnCodeT stringDuplicate(const char *HTTP2_CONST_RESTRICT_SPECIFIER string, char **newString) {
    size_t size;

    if (string == NULL) {
        *newString = NULL;
        return HTTP2_SUCCESS;
    }

    size = strlen(string) + 1;

    *newString = http2Malloc(size);

    if (*newString == NULL) {
        return HTTP2_NO_MEMORY;
    }

    memcpy(*newString, string, size);

    return HTTP2_SUCCESS;
}
