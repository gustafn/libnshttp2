/*
MIT License
-----------

Copyright (c) 2021 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/utility/memory.h>
#include <nshttp2/utility/nghttp2.h>
#include <nshttp2/stddef.h>

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L

static void *http2MallocDefault(size_t nmemb, void *UNUSED(userData)) {
    return http2Malloc(nmemb);
}

static void *http2CallocDefault(size_t nmemb, size_t size, void *UNUSED(userData)) {
    return http2Calloc(nmemb * size);
}

static void http2FreeDefault(void *ptr, void *UNUSED(memUserData)) {
    http2Free(ptr);
}

static void *http2ReallocDefault(void *ptr, size_t size, void *UNUSED(memUserData)) {
    return http2Realloc(ptr, size);
}

nghttp2_mem NGHTTP2_MEM = {
        .malloc = http2MallocDefault,
        .calloc = http2CallocDefault,
        .free = http2FreeDefault,
        .realloc = http2ReallocDefault,
        .mem_user_data = NULL
};

#else

static nghttp2_mem NGHTTP2_MEM;

#endif

Http2ReturnCodeT ngHttp2GetInflater(nghttp2_hd_inflater **inflater) {
    nghttp2_hd_inflate_new2(inflater, (nghttp2_mem *) &NGHTTP2_MEM);

    if (inflater == NULL) {
        return HTTP2_NO_MEMORY;
    } else {
        return HTTP2_SUCCESS;
    }
}

Http2ReturnCodeT ngHttp2GetDeflater(nghttp2_hd_deflater **deflater,
                                    const uint32_t headerTableSize) {
    nghttp2_hd_deflate_new2(deflater, headerTableSize, (nghttp2_mem *) &NGHTTP2_MEM);

    if (deflater == NULL) {
        return HTTP2_NO_MEMORY;
    } else {
        return HTTP2_SUCCESS;
    }
}

void ngHttp2SetupMemoryAllocators(nghttp2_mem *nghttp2Mem) {
    NGHTTP2_MEM = *nghttp2Mem;
}
