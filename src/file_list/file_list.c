/*
MIT License
-----------

Copyright (c) 2021 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/utility/file_list.h>
#include <nshttp2/utility/memory.h>
#include <nshttp2/stddef.h>
#include <nshttp2/utility/string_utils.h>
#include <string.h>

static Http2ReturnCodeT http2AllocateFileListElement(Http2FileListElementT **fileListElement,
                                                     Http2OutgoingFileT *HTTP2_CONST_RESTRICT_SPECIFIER outgoingFile,
                                                     Http2FileListElementT *next) {
    *fileListElement = http2Malloc(sizeof(Http2FileListElementT));

    if ((*fileListElement) == NULL) {
        return HTTP2_NO_MEMORY;
    }

    (*fileListElement)->next = next;
    (*fileListElement)->outgoingFile = outgoingFile;

    return HTTP2_SUCCESS;
}

static void http2DeallocateFileListElement(Http2FileListElementT *HTTP2_CONST_RESTRICT_SPECIFIER fileListElement) {
    http2OutgoingFileDeallocate(fileListElement->outgoingFile);
    http2Free(fileListElement);
}

Http2ReturnCodeT http2FileListAllocate(Http2FileListT **fileList,
                                       char *HTTP2_CONST_RESTRICT_SPECIFIER fileName,
                                       const size_t offset,
                                       const size_t size) {
    Http2OutgoingFileT *outgoingFile;
    Http2FileListElementT *fileListElement;

    switch (http2OutgoingFileAllocate(&outgoingFile, fileName, offset, size)) {
        case HTTP2_NO_MEMORY:
            return HTTP2_NO_MEMORY;
        default:
            break;
    }

    switch (http2AllocateFileListElement(&fileListElement, outgoingFile, NULL)) {
        case HTTP2_NO_MEMORY:
            http2OutgoingFileDeallocate(outgoingFile);
            return HTTP2_NO_MEMORY;
        default:
            break;
    }

    *fileList = http2Calloc(sizeof(Http2FileListT));

    if ((*fileList) == NULL) {
        http2OutgoingFileDeallocate(outgoingFile);
        http2DeallocateFileListElement(fileListElement);
        return HTTP2_NO_MEMORY;
    }

    (*fileList)->head = fileListElement;
    (*fileList)->tail = fileListElement;
    (*fileList)->length = 1u;

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2OutgoingFileAllocate(Http2OutgoingFileT **outgoingFile,
                                           char *HTTP2_CONST_RESTRICT_SPECIFIER fileName,
                                           const size_t offset,
                                           const size_t size) {
    *outgoingFile = http2Malloc(sizeof(Http2OutgoingFileT));

    if ((*outgoingFile) == NULL) {
        return HTTP2_NO_MEMORY;
    }

    switch (stringDuplicate(fileName, &(*outgoingFile)->fileName)) {
        case HTTP2_NO_MEMORY:
            http2Free(*outgoingFile);
            return HTTP2_NO_MEMORY;
        default:
            break;
    }

    (*outgoingFile)->offset = offset;
    (*outgoingFile)->size = size;

    return HTTP2_SUCCESS;
}

void http2OutgoingFileDeallocate(Http2OutgoingFileT *outgoingFile) {
    http2Free(outgoingFile->fileName);
    http2Free(outgoingFile);
}

Http2ReturnCodeT http2FileListAppend(Http2FileListT *HTTP2_CONST_RESTRICT_SPECIFIER fileList,
                                     char *HTTP2_CONST_RESTRICT_SPECIFIER fileName,
                                     const size_t offset,
                                     const size_t size) {
    Http2OutgoingFileT *outgoingFile;
    Http2FileListElementT *fileListElement;

    switch (http2OutgoingFileAllocate(&outgoingFile, fileName, offset, size)) {
        case HTTP2_NO_MEMORY:
            return HTTP2_NO_MEMORY;
        default:
            break;
    }

    switch (http2AllocateFileListElement(&fileListElement, outgoingFile, NULL)) {
        case HTTP2_NO_MEMORY:
            http2OutgoingFileDeallocate(outgoingFile);
            return HTTP2_NO_MEMORY;
        default:
            break;
    }

    switch (fileList->length) {
        case 0u:
            fileList->head = fileListElement;
            fileList->tail = fileListElement;
            fileList->length = 1u;
            break;
        default:
            fileList->tail->next = fileListElement;
            fileList->tail = fileListElement;
            ++fileList->length;
            break;
    }

    fileList->bufferSize += (fileListElement->outgoingFile->size - fileListElement->outgoingFile->offset);

    return HTTP2_SUCCESS;
}

void http2FileListRemoveElementFromStart(Http2FileListT *HTTP2_CONST_RESTRICT_SPECIFIER fileList) {
    if (fileList->head == NULL) {
        return;
    }

    if (fileList->head->next != NULL) {
        Http2FileListElementT *const element = fileList->head;
        fileList->head = fileList->head->next;
        http2Free(element);
    } else {
        fileList->head = NULL;
    }

    --fileList->length;

    if (fileList->length == 0u) {
        memset(fileList, 0u, sizeof(Http2FileListT));
    }
}

void http2FileListDeallocate(Http2FileListT *HTTP2_CONST_RESTRICT_SPECIFIER fileList) {
    Http2FileListElementT *element = fileList->head;

    while (element != NULL) {
        Http2FileListElementT *const nextElement = element->next;

        http2DeallocateFileListElement(element);

        element = nextElement;
    }
}
