/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

/**
 * Provides functions and structures needed
 * to parse frames from a buffer into a list.
 */

#include <nshttp2/stddef.h>
#include <nshttp2/connection.h>
#include <nshttp2/utility/nghttp2.h>

Http2ReturnCodeT http2ConnectionAllocate(Http2ConnectionT **http2Connection,
                                         Http2ConnectionSettingsT *HTTP2_CONST_RESTRICT_SPECIFIER connectionSettings) {
    /* Initialize the connection container */
    *http2Connection = http2Calloc(sizeof(Http2ConnectionT));

    if (*http2Connection == NULL) {
        return HTTP2_NO_MEMORY;
    }

    /* Initialize the pthread mutex */
    pthread_mutex_init(&(*http2Connection)->mutex, NULL);

    /* Initialize the stream hashmap */
    switch (streamHashmapAllocate(&(*http2Connection)->streamIdToStreamMap)) {
        case HTTP2_NO_MEMORY:
            http2Free(*http2Connection);
            return HTTP2_NO_MEMORY;
        case HTTP2_INTERNAL_ERROR:
            http2Free(*http2Connection);
            return HTTP2_INTERNAL_ERROR;
        default:
            break;
    }

    /* Fixed settings */
    (*http2Connection)->receivedSettings = connectionSettings->receivedSettings;
    (*http2Connection)->sentSettings = connectionSettings->sentSettings;

    (*http2Connection)->maxIncomingDataInMemory = connectionSettings->maxIncomingDataInMemory;

    switch (stringDuplicate(connectionSettings->temporaryFileTemplate,
                            &(*http2Connection)->temporaryFileTemplate)) {
        case HTTP2_SUCCESS:
            break;
        default:
            http2Free(*http2Connection);
            streamHashmapDeallocate((*http2Connection)->streamIdToStreamMap);
            break;
    }

    (*http2Connection)->isClient = connectionSettings->isClient;

    ngHttp2GetDeflater(&(*http2Connection)->deflater, (*http2Connection)->receivedSettings.headerTableSize);

    ngHttp2GetInflater(&(*http2Connection)->inflater);

    /* Initial values for the HTTP/2 connection */
    (*http2Connection)->remainingSendWindow =
            (int_fast32_t) connectionSettings->receivedSettings.initialWindowSize;
    (*http2Connection)->remainingReceiveWindow =
            (int_fast32_t) connectionSettings->sentSettings.initialWindowSize;

    if (connectionSettings->isClient) {
        (*http2Connection)->lastReceivedStreamId = 1u;
    } else {
        (*http2Connection)->lastSentStreamId = 1u;
    }

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2ConnectionProcessFrame(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                             Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                             const bool sent) {
    if (http2FrameIsStreamFrame(frame)) {
        return http2ConnectionProcessNonStreamFrame(http2Connection, frame, sent);
    } else if (http2FrameIsNonControlFrame(frame)) {
        return http2ConnectionProcessStreamFrame(http2Connection, frame, sent);
    } else {
        return HTTP2_UNSUPPORTED_FRAME_TYPE;
    }
}

Http2ReturnCodeT http2ConnectionProcessNonStreamFrame(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                                      Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                                      const bool sent) {
    switch (frame->header.type) {
        case GOAWAY_FRAME_TYPE:
        case PING_FRAME_TYPE:
            /* No action is taken on ping frame types. */
            return HTTP2_SUCCESS;
        case WINDOW_UPDATE_FRAME_TYPE:
            if (sent) {
                http2Connection->remainingReceiveWindow +=
                        (int_fast32_t) frame->payload.http2WindowUpdatePayload->windowSizeIncrement;
            } else {
                http2Connection->remainingSendWindow +=
                        (int_fast32_t) frame->payload.http2WindowUpdatePayload->windowSizeIncrement;
            }

            return HTTP2_SUCCESS;
        case PUSH_PROMISE_FRAME_TYPE:
            return HTTP2_SUCCESS;
        default:
            return HTTP2_UNSUPPORTED_FRAME_TYPE;
    }
}

Http2ReturnCodeT http2ConnectionCreateGoaway(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                             const uint32_t errorCode,
                                             uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER buffer) {
    Http2GoawayFramePayloadT goaway;
    Http2FrameT http2_frame;

    goaway.lastStreamId = (uint32_t) http2Connection->lastSentStreamId;
    goaway.errorCode = errorCode;
    goaway.additionalDebugData = NULL;

    http2_frame.header.streamId = 0u;
    http2_frame.header.type = GOAWAY_FRAME_TYPE;
    http2_frame.header.length = HTTP2_GOAWAY_MINIMUM_PAYLOAD_LEN;
    http2_frame.header.flags = DEFAULT_FRAME_FLAG;
    http2_frame.payload.http2GoawayFramePayload = &goaway;

    http2FrameToBytes(&http2_frame,
                      buffer,
                      NULL);

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2ConnectionProcessStreamFrame(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                                   Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                                   bool sent) {
    Http2StreamT *stream;

    switch (http2ConnectionStreamIdState(http2Connection, frame, sent)) {
        case HTTP2_STREAM_ID_TOO_LARGE:
            return HTTP2_STREAM_ID_TOO_LARGE;
        default:
            break;
    }

    stream = NULL;

    streamHashmapGet(http2Connection->streamIdToStreamMap, frame->header.streamId, &stream);

    if (stream != NULL) {
        switch (http2StreamFrameGetError(stream, frame, sent)) {
            case PROTOCOL_ERROR_ERROR_CODE:
                return HTTP2_PROTOCOL_ERROR;
            case STREAM_CLOSED_ERROR_CODE:
                return HTTP2_STREAM_CLOSED_ERROR;
            default:
                break;
        }
    }

    switch (frame->header.type) {
        case DATA_FRAME_TYPE:
            switch (http2ConnectionProcessDataFrame(http2Connection, frame, stream, sent)) {
                case HTTP2_SUCCESS:
                    break;
                case HTTP2_NO_MEMORY:
                    return HTTP2_NO_MEMORY;
                default:
                    return HTTP2_INTERNAL_ERROR;
            }
            break;
        case CONTINUATION_FRAME_TYPE:
            switch (http2ConnectionProcessContinuationFrame(http2Connection, frame, sent)) {
                case HTTP2_SUCCESS:
                    break;
                case HTTP2_NO_MEMORY:
                    return HTTP2_NO_MEMORY;
                default:
                    return HTTP2_INTERNAL_ERROR;
            }
            break;
        case HEADERS_FRAME_TYPE:
            switch (http2ConnectionProcessHeadersFrame(http2Connection, frame, sent)) {
                case HTTP2_SUCCESS:
                    break;
                case HTTP2_NO_MEMORY:
                    return HTTP2_NO_MEMORY;
                default:
                    return HTTP2_INTERNAL_ERROR;
            }
            break;
        case PRIORITY_FRAME_TYPE:
            switch (http2ConnectionProcessPriority(http2Connection, frame)) {
                case HTTP2_SUCCESS:
                    break;
                case HTTP2_NO_MEMORY:
                    return HTTP2_NO_MEMORY;
                default:
                    return HTTP2_INTERNAL_ERROR;
            }
            break;
        case WINDOW_UPDATE_FRAME_TYPE:
            switch (http2ConnectionProcessWindowUpdateFrameOnStream(http2Connection, frame, sent)) {
                case HTTP2_SUCCESS:
                    break;
                case HTTP2_NO_MEMORY:
                    return HTTP2_NO_MEMORY;
                default:
                    return HTTP2_INTERNAL_ERROR;
            }
            break;
        case RST_STREAM_FRAME_TYPE:
            switch (http2ConnectionProcessRstStreamFrame(http2Connection, frame)) {
                case HTTP2_SUCCESS:
                    break;
                case HTTP2_NO_MEMORY:
                    return HTTP2_NO_MEMORY;
                default:
                    return HTTP2_INTERNAL_ERROR;
            }
            break;
    }

    if (frame->header.flags & END_STREAM_FRAME_FLAG) {
        if (stream != NULL) {
            switch (stream->state) {
                case IDLE:
                case RESERVED_LOCAL:
                case RESERVED_REMOTE:
                    break;
                case OPEN:
                    if (!sent) {
                        http2StreamSetState(stream, HALF_CLOSED_REMOTE);
                    } else {
                        http2StreamSetState(stream, HALF_CLOSED_LOCAL);
                    }
                    break;
                case HALF_CLOSED_LOCAL:
                    if (!sent) {
                        stream->state = CLOSED;
                    }
                    break;
                case HALF_CLOSED_REMOTE:
                    if (sent) {
                        stream->state = CLOSED;
                    }
                    break;
                case CLOSED:
                    break;
            }
        }
    }

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT
http2ConnectionProcessWindowUpdateFrameOnStream(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                                Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                                const bool sent) {
    Http2StreamT *stream = NULL;
    streamHashmapGet(http2Connection->streamIdToStreamMap, frame->header.streamId, &stream);

    if (stream == NULL) {
        return HTTP2_PROTOCOL_ERROR;
    }

    if (!sent) {
        stream->remainingSendWindow += frame->payload.http2WindowUpdatePayload->windowSizeIncrement;
    } else {
        stream->remainingReceiveWindow += frame->payload.http2WindowUpdatePayload->windowSizeIncrement;
    }

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2ConnectionProcessDataFrame(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                                 Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                                 Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER stream,
                                                 const bool sent) {
    if (stream == NULL) {
        /* Data frames cannot be sent or received if a stream does not exist, therefore we just ignore it. */
        return HTTP2_PROTOCOL_ERROR;
    } else {

        switch (stream->state) {
            case IDLE:
            case RESERVED_LOCAL:
            case RESERVED_REMOTE:
            case CLOSED:
                return HTTP2_PROTOCOL_ERROR;
            case HALF_CLOSED_LOCAL:
                return HTTP2_STREAM_CLOSED_ERROR;
            case HALF_CLOSED_REMOTE:
            case OPEN:
                /* This is the only state where we are allowed to send and receive data frames */
                if (sent) {
                    http2Connection->remainingSendWindow -= frame->header.length;
                    stream->remainingSendWindow -= frame->header.length;
                } else {
                    http2StreamAppendIncomingData(stream,
                                                  frame->payload.http2DataFramePayload,
                                                  frame->header.length,
                                                  http2Connection->temporaryFileTemplate);

                    http2Connection->remainingReceiveWindow -= frame->header.length;
                    stream->remainingReceiveWindow -= frame->header.length;
                }

                return HTTP2_SUCCESS;
        }
    }

    return HTTP2_SUCCESS;
}

static Http2ReturnCodeT http2ConnectionProcessIncomingHeaderBlock(Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                                                  Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER stream) {
    if (!http2FrameIsLastHeader(frame)) {
        stream->flags |= HTTP2_STREAM_CONTINUATION_EXPECTED;
    }

    return http2StreamAddHeaderBlock(stream, frame);
}

Http2ReturnCodeT http2ConnectionProcessHeadersFrame(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                                    Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                                    const bool sent) {
    Http2StreamT *stream = NULL;

    const bool hasPriority = http2FrameHasPriority(frame);

    if (hasPriority) {
        http2ConnectionGetOrCreateStream(http2Connection,
                                         frame->header.streamId,
                                         frame->payload.http2HeadersFramePayload->priority.weight,
                                         &stream);
    } else {
        http2ConnectionGetOrCreateStream(http2Connection,
                                         frame->header.streamId,
                                         0u,
                                         &stream);
    }

    if (stream == NULL) {
        return HTTP2_PROTOCOL_ERROR;
    }

    switch (stream->state) {
        case IDLE:
            stream->state = OPEN;

            if (sent) {
                return HTTP2_SUCCESS;
            } else {
                switch (http2ConnectionProcessIncomingHeaderBlock(frame, stream)) {
                    default:
                        return HTTP2_INTERNAL_ERROR;
                    case HTTP2_NO_MEMORY:
                        return HTTP2_NO_MEMORY;
                    case HTTP2_SUCCESS:
                        /* We need to handle the priority if there is one */
                        if (hasPriority) {
                            return http2ConnectionProcessPriority(http2Connection,
                                                                  frame);
                        } else {
                            return HTTP2_SUCCESS;
                        }
                }
            }
        case RESERVED_LOCAL:
            if (!sent) {
                stream->state = HALF_CLOSED_LOCAL;

                return http2ConnectionProcessIncomingHeaderBlock(frame,
                                                                 stream);
            } else {
                return HTTP2_PROTOCOL_ERROR;
            }

        case RESERVED_REMOTE:
            if (sent) {
                stream->state = HALF_CLOSED_REMOTE;
                return HTTP2_SUCCESS;
            } else {
                return HTTP2_PROTOCOL_ERROR;
            }
        case OPEN:
        case HALF_CLOSED_LOCAL:
        case HALF_CLOSED_REMOTE:
        case CLOSED:
            return HTTP2_PROTOCOL_ERROR;
    }

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2ConnectionProcessPriority(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                                Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame) {
    Http2FramePriorityT *priority;
    Http2StreamT *stream;
    Http2StreamT *parent;

    switch (frame->header.type) {
        case HEADERS_FRAME_TYPE:
            /* If a priority exists there should be a flag set */
            if (frame->header.flags & PRIORITY_FRAME_FLAG) {
                priority = &frame->payload.http2HeadersFramePayload->priority;
            } else {
                priority = NULL;
            }

            break;
        case PRIORITY_FRAME_TYPE:
            priority = frame->payload.http2PriorityFramePayload;
            break;
        default:
            priority = NULL;
            break;
    }

    /* If the priority field is empty or not set in the flags, ignore it */
    if (priority == NULL) {
        return HTTP2_SUCCESS;
    }

    /* Retrieve or create the stream in the priority */
    stream = NULL;

    switch (http2ConnectionGetOrCreateStream(http2Connection,
                                             frame->header.streamId,
                                             0u,
                                             &stream)) {
        case HTTP2_NO_MEMORY:
            return HTTP2_NO_MEMORY;
        default:
            break;
        case HTTP2_STREAM_MAP_EMPTY:
            return HTTP2_PROTOCOL_ERROR;
    }

    /** If the stream could not be found or created
     * the stream id is not valid so we exit with
     * the appropriate error code.
     */
    if (stream == NULL) {
        return HTTP2_PROTOCOL_ERROR;
    }

    parent = NULL;

    switch (http2ConnectionGetOrCreateStream(http2Connection,
                                             priority->dependency,
                                             0u,
                                             &parent)) {
        case HTTP2_NO_MEMORY:
            http2ConnectionRemoveAndDeallocateStream(http2Connection, (uint32_t) stream->streamId);
            return HTTP2_NO_MEMORY;
        default:
            break;
    }

    /**
     * If there is a parent, we have to set its children
     * correctly, otherwise we are done.
     */
    if (parent != NULL) {
        if (priority->exclusive) {
            if (parent->children == NULL) {
                http2StreamAddChild(parent, stream);
            } else {
                size_t children_count;
                size_t i = 0u;
                Http2StreamT **streams;
                Http2StreamT *new_stream;

                /** Since we have a parent and it is exclusive we
                 * need to move each child to be the child of the new stream
                 * and make the stream the only child of the parent
                 */

                children_count = kh_size(parent->children->hashmap);

                streams = http2Malloc(sizeof(Http2StreamT *) * parent->children->hashmap->size);
                kh_foreach_value(parent->children->hashmap, new_stream, {
                    streams[i++] = new_stream;
                })

                for (i = 0; i < children_count; ++i) {
                    http2StreamAddChild(stream, streams[i]);
                    streams[i]->parent = stream;
                    http2StreamRemoveChild(parent, streams[i]);
                }

                http2Free(streams);
            }
        } else {
            http2StreamAddChild(parent, stream);
        }
    }

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2ConnectionCreateStream(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                             const uint32_t streamId,
                                             const uint8_t weight,
                                             Http2StreamT **stream) {
    switch (http2StreamAllocate(stream,
                                streamId,
                                weight,
                                http2Connection->receivedSettings.initialWindowSize,
                                http2Connection->sentSettings.initialWindowSize,
                                http2Connection->maxIncomingDataInMemory)) {
        case HTTP2_NO_MEMORY:
            return HTTP2_NO_MEMORY;
        default:
            ++http2Connection->activeStreams;
            streamHashmapInsertWithStreamId(http2Connection->streamIdToStreamMap, streamId, *stream);

            return HTTP2_SUCCESS;
    }
}

Http2ReturnCodeT http2ConnectionGetStream(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                          const uint32_t streamId,
                                          Http2StreamT **stream) {
    if (streamId == 0u) {
        *stream = NULL;
        return HTTP2_SUCCESS;
    }

    streamHashmapGet(http2Connection->streamIdToStreamMap,
                     streamId,
                     stream);

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2ConnectionGetOrCreateStream(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                                  const uint32_t streamId,
                                                  const uint8_t weight,
                                                  Http2StreamT **stream) {
    if (streamId == 0u) {
        *stream = NULL;
        return HTTP2_SUCCESS;
    }

    streamHashmapGet(http2Connection->streamIdToStreamMap,
                     streamId,
                     stream);

    if (*stream == NULL) {
        return http2ConnectionCreateStream(http2Connection, streamId, weight, stream);
    } else {
        return HTTP2_SUCCESS;
    }
}

void http2ConnectionRemoveAndDeallocateStream(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                              const uint32_t streamId) {
    Http2StreamT *stream = NULL;

    streamHashmapGet(http2Connection->streamIdToStreamMap,
                     streamId,
                     &stream);

    if (stream == NULL) {
        return;
    }

    streamHashmapRemove(http2Connection->streamIdToStreamMap, streamId);
    --http2Connection->activeStreams;

    http2StreamDeallocate(stream);
}

void http2ConnectionDeallocateAndClose(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection) {
    if (http2Connection == NULL) {
        return;
    }

    if (http2Connection->streamIdToStreamMap != NULL &&
        http2Connection->streamIdToStreamMap->hashmap != NULL) {
        if (kh_size(http2Connection->streamIdToStreamMap->hashmap) > 0u) {
            /* Remove all streams and the hashmap containing them */
            Http2StreamT *stream = NULL;
            kh_foreach_value(http2Connection->streamIdToStreamMap->hashmap, stream, {
                if (stream->incomingHeadersBuffer != NULL) {
                    http2BufferDeallocate(stream->incomingHeadersBuffer);
                }

                if (stream->siblings != NULL) {
                    kh_destroy_hashmap(stream->siblings->hashmap);
                    http2Free(stream->siblings);
                }

                if (stream->children != NULL) {
                    kh_destroy_hashmap(stream->children->hashmap);
                    http2Free(stream->children);
                }

                http2TemporaryBufferReset(&stream->incomingDataBuffer);

                if (stream->outgoingDataMemoryBuffer != NULL) {
                    http2BufferDeallocate(stream->outgoingDataMemoryBuffer);
                }

                pthread_mutex_destroy(&(*stream).mutex);

                http2Free(stream);
            })
        }

        streamHashmapDeallocate(http2Connection->streamIdToStreamMap);
    }

    /* Run the connection close function */
    if (http2Connection->closeConnectionFunction != NULL) {
        http2Connection->closeConnectionFunction();
    }

    if (http2Connection->temporaryFileTemplate != NULL) {
        http2Free(http2Connection->temporaryFileTemplate);
    }

    if (http2Connection->deflater != NULL) {
        nghttp2_hd_deflate_del(http2Connection->deflater);
    }

    if (http2Connection->inflater != NULL) {
        nghttp2_hd_inflate_del(http2Connection->inflater);
    }

    /* Destroy the mutex */
    pthread_mutex_destroy(&http2Connection->mutex);

    /* Free the connection */
    http2Free(http2Connection);
}

Http2ReturnCodeT
http2ConnectionProcessContinuationFrame(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                        Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                        const bool sent) {
    Http2StreamT *stream = NULL;
    streamHashmapGet(http2Connection->streamIdToStreamMap, frame->header.streamId, &stream);

    if (stream != NULL) {
        if (!sent) {
            const Http2ReturnCodeT return_code = http2ConnectionProcessIncomingHeaderBlock(frame,
                                                                                           stream);

            if (return_code != HTTP2_SUCCESS) {
                return return_code;
            }
        }

        if (frame->header.flags & END_HEADERS_FRAME_FLAG) {
            stream->flags |= HTTP2_STREAM_CONTINUATION_EXPECTED;
        }
    }

    return HTTP2_SUCCESS;
}

/*
TODO neeeds to be implemented
Http2ReturnCodeT
http2ConnectionProcessPushPromiseFrame(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER UNUSED(http2_connection),
                                       Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER UNUSED(frame),
                                       const bool UNUSED(sent)) {
    return HTTP2_SUCCESS;
}
*/

Http2ReturnCodeT http2ConnectionProcessRstStreamFrame(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                                      Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame) {
    Http2StreamT *stream = NULL;
    streamHashmapGet(http2Connection->streamIdToStreamMap, frame->header.streamId, &stream);

    if (stream != NULL) {
        switch (stream->state) {
            case RESERVED_LOCAL:
            case RESERVED_REMOTE:
            case OPEN:
            case HALF_CLOSED_REMOTE:
            case HALF_CLOSED_LOCAL:
                http2StreamSetState(stream, CLOSED);
                break;
            case IDLE:
            case CLOSED:
                break;
        }
    }

    return HTTP2_SUCCESS;
}

Http2ReturnCodeT http2ConnectionStreamIdState(const Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                              const Http2FrameT *HTTP2_CONST_RESTRICT_SPECIFIER frame,
                                              const bool sent) {
    /**
     * Here we check if the last available stream ID has been used up.
     * In this case we need to close the connection.
     */
    if (http2Connection->isClient) {
        if (sent) {
            if (frame->header.streamId > CLIENT_STREAM_ID_MAX) {
                return HTTP2_STREAM_ID_TOO_LARGE;
            }
        } else {
            if (frame->header.streamId > SERVER_STREAM_ID_MAX) {
                return HTTP2_STREAM_ID_TOO_LARGE;
            }
        }
    } else {
        if (sent) {
            if (frame->header.streamId > SERVER_STREAM_ID_MAX) {
                return HTTP2_STREAM_ID_TOO_LARGE;
            }
        } else {
            if (frame->header.streamId > CLIENT_STREAM_ID_MAX) {
                return HTTP2_STREAM_ID_TOO_LARGE;
            }
        }
    }

    return HTTP2_SUCCESS;
}

int_fast32_t http2ConnectionGetRemainingSendWindowForStream(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                                            Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection) {
    const int_fast32_t remainingConnectionSendWindow = http2Connection->remainingSendWindow;
    const int_fast32_t windowSize = http2Stream->remainingSendWindow;

    if (remainingConnectionSendWindow > windowSize) {
        return windowSize;
    } else {
        return remainingConnectionSendWindow;
    }
}

static void fillMemoryBufferWithStreamData(const size_t lengthOfDataToSend,
                                           uint8_t *HTTP2_CONST_RESTRICT_SPECIFIER dataBuffer,
                                           Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                           size_t *HTTP2_CONST_RESTRICT_SPECIFIER bufferLength) {
    Http2BufferT *memoryBuffer = http2Stream->outgoingDataMemoryBuffer;
    size_t storedData;

    if (memoryBuffer != NULL) {
        if (lengthOfDataToSend == memoryBuffer->length) {
            memcpy(dataBuffer, memoryBuffer->buffer, lengthOfDataToSend);
            http2BufferDeallocate(memoryBuffer);
            http2Stream->outgoingDataMemoryBuffer = NULL;
            *bufferLength = lengthOfDataToSend;
            return;
        } else if (lengthOfDataToSend < memoryBuffer->length) {
            memcpy(dataBuffer, memoryBuffer->buffer, lengthOfDataToSend);
            http2BufferMoveByOffset(memoryBuffer, lengthOfDataToSend);
            *bufferLength = lengthOfDataToSend;
            return;
        } else {
            memcpy(dataBuffer, memoryBuffer->buffer, memoryBuffer->length);

            storedData = memoryBuffer->length;

            http2BufferDeallocate(memoryBuffer);
            http2Stream->outgoingDataMemoryBuffer = NULL;
        }

        if (storedData != lengthOfDataToSend) {
            http2Realloc(dataBuffer, storedData);
        }

        *bufferLength = storedData;
    }
}

Http2ReturnCodeT http2ConnectionGetFramedStreamData(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                                    Http2FrameListT **frameList,
                                                    Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                                    const size_t chunkSize,
                                                    const bool isEndOfStream) {
    int_fast32_t remainingConnectionSendWindow;
    uint32_t maxFrameSize;
    size_t outgoingBufferLength;
    Http2BufferT *memoryBuffer;
    size_t remainingWindow;
    size_t lengthOfDataToSend;

    memoryBuffer = http2Stream->outgoingDataMemoryBuffer;
    outgoingBufferLength = 0u;

    if (memoryBuffer != NULL) {
        outgoingBufferLength += memoryBuffer->length;
    }

    if (outgoingBufferLength == 0u) {
        *frameList = NULL;
        return HTTP2_SUCCESS;
    }

    remainingConnectionSendWindow = http2Connection->remainingSendWindow;
    maxFrameSize = http2Connection->receivedSettings.maxFrameSize;

    if ((remainingConnectionSendWindow <= 0) || (http2Stream->remainingSendWindow <= 0)) {
        *frameList = NULL;
        return HTTP2_SUCCESS;
    } else if (remainingConnectionSendWindow > http2Stream->remainingSendWindow) {
        remainingWindow = (size_t) http2Stream->remainingSendWindow;
    } else {
        remainingWindow = (size_t) remainingConnectionSendWindow;
    }

    if (chunkSize < remainingWindow) {
        remainingWindow = chunkSize;
    }


    if (remainingWindow <= outgoingBufferLength) {
        lengthOfDataToSend = remainingWindow;
    } else {
        lengthOfDataToSend = outgoingBufferLength;
    }

    switch (http2FrameListAllocateEmpty(frameList)) {
        case HTTP2_FRAME_LIST_NOT_ENOUGH_MEMORY:
            return HTTP2_NO_MEMORY;
        default:
            break;
    }

    if (lengthOfDataToSend <= (size_t) maxFrameSize) {
        Http2FrameT *frame;
        size_t bufferLength;
        uint8_t *dataBuffer = http2Malloc(lengthOfDataToSend);

        if (dataBuffer == NULL) {
            http2FrameListDeallocate(*frameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
            return HTTP2_NO_MEMORY;
        }

        frame = http2Malloc(sizeof(Http2FrameT));

        if (frame == NULL) {
            http2Free(dataBuffer);
            http2FrameListDeallocate(*frameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
            return HTTP2_NO_MEMORY;
        }

        frame->header.length = (uint32_t) lengthOfDataToSend;
        frame->header.streamId = (uint32_t) http2Stream->streamId;
        frame->header.flags = isEndOfStream ? END_STREAM_FRAME_FLAG : DEFAULT_FRAME_FLAG;
        frame->header.type = DATA_FRAME_TYPE;
        frame->payload.http2DataFramePayload = dataBuffer;

        switch (http2FrameListAppend(*frameList, frame)) {
            case HTTP2_FRAME_LIST_SUCCESS:
                break;
            default:
                http2Free(dataBuffer);
                http2Free(frame);
                http2FrameListDeallocate(*frameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
                return HTTP2_NO_MEMORY;
        }

        bufferLength = 0u;
        fillMemoryBufferWithStreamData(lengthOfDataToSend, dataBuffer, http2Stream, &bufferLength);
        frame->header.length = (uint32_t) bufferLength;
    } else {
        uint8_t *dataBuffer;
        size_t sentData = 0u;
        size_t bufferLength;
        Http2FrameT *frame;

        while (lengthOfDataToSend > (size_t) maxFrameSize) {
            Http2FrameT *temporaryFrame;
            size_t temporaryBufferLength;
            uint8_t *temporaryDataBuffer = http2Malloc(maxFrameSize);

            if (temporaryDataBuffer == NULL) {
                http2FrameListDeallocate(*frameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
                return HTTP2_NO_MEMORY;
            }

            temporaryFrame = http2Malloc(sizeof(Http2FrameT));

            if (temporaryFrame == NULL) {
                http2Free(temporaryDataBuffer);
                http2FrameListDeallocate(*frameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
                return HTTP2_NO_MEMORY;
            }

            temporaryFrame->header.length = maxFrameSize;
            temporaryFrame->header.streamId = (uint32_t) http2Stream->streamId;
            temporaryFrame->header.flags = DEFAULT_FRAME_FLAG;
            temporaryFrame->header.type = DATA_FRAME_TYPE;
            temporaryFrame->payload.http2DataFramePayload = temporaryDataBuffer;

            switch (http2FrameListAppend(*frameList, temporaryFrame)) {
                case HTTP2_FRAME_LIST_SUCCESS:
                    break;
                default:
                    http2Free(temporaryDataBuffer);
                    http2Free(temporaryFrame);
                    http2FrameListDeallocate(*frameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
                    return HTTP2_NO_MEMORY;
            }

            temporaryBufferLength = 0u;
            fillMemoryBufferWithStreamData(maxFrameSize,
                                           temporaryDataBuffer,
                                           http2Stream,
                                           &temporaryBufferLength);

            lengthOfDataToSend -= maxFrameSize;
            sentData += maxFrameSize;
        }

        dataBuffer = http2Malloc(lengthOfDataToSend);

        if (dataBuffer == NULL) {
            http2FrameListDeallocate(*frameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
            return HTTP2_NO_MEMORY;
        }

        bufferLength = 0u;
        fillMemoryBufferWithStreamData(maxFrameSize, dataBuffer, http2Stream, &bufferLength);

        frame = http2Malloc(sizeof(Http2FrameT));

        if (frame == NULL) {
            http2Free(dataBuffer);
            http2Free(frame);
            http2FrameListDeallocate(*frameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
            return HTTP2_NO_MEMORY;
        }

        frame->header.length = (uint32_t) lengthOfDataToSend;
        frame->header.streamId = (uint32_t) http2Stream->streamId;
        frame->header.flags = isEndOfStream ? END_STREAM_FRAME_FLAG : DEFAULT_FRAME_FLAG;
        frame->header.type = DATA_FRAME_TYPE;
        frame->payload.http2DataFramePayload = dataBuffer;

        switch (http2FrameListAppend(*frameList, frame)) {
            case HTTP2_FRAME_LIST_SUCCESS:
                break;
            default:
                http2Free(dataBuffer);
                http2Free(frame);
                http2FrameListDeallocate(*frameList, HTTP2_FRAME_LIST_DEALLOCATE_FRAMES);
                return HTTP2_NO_MEMORY;
        }
    }

    return HTTP2_SUCCESS;
}

HTTP2_INLINE_SPECIFIER Http2ReturnCodeT
http2ConnectionStreamDecodeHeaders(Http2StreamT *HTTP2_CONST_RESTRICT_SPECIFIER http2Stream,
                                   Http2HeaderListT **headerList,
                                   Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection) {
    return http2StreamDecodeHeaders(http2Stream, headerList, http2Connection->inflater);
}

HTTP2_INLINE_SPECIFIER Http2ReturnCodeT
http2ConnectionHeadersEncode(Http2HeaderListT *HTTP2_CONST_RESTRICT_SPECIFIER http2HeaderList,
                             Http2BufferT **buffer,
                             Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection) {
    return http2HeadersEncode(http2HeaderList, buffer, http2Connection->deflater);
}

Http2ReturnCodeT
http2ConnectionProcessFrameList(Http2ConnectionT *HTTP2_CONST_RESTRICT_SPECIFIER http2Connection,
                                Http2FrameListT *HTTP2_CONST_RESTRICT_SPECIFIER frameList,
                                const bool sent) {
    Http2FrameListElementT *frameListElement = frameList->head;

    while (frameListElement != NULL) {
        switch (http2ConnectionProcessFrame(http2Connection, frameListElement->http2Frame, sent)) {
            case HTTP2_PROTOCOL_ERROR:
                return HTTP2_PROTOCOL_ERROR;
            case HTTP2_NO_MEMORY:
                return HTTP2_NO_MEMORY;
            default:
                break;
        }

        frameListElement = frameListElement->next;
    }

    return HTTP2_SUCCESS;
}
