/*
MIT License
-----------

Copyright (c) 2020 Filip Minic (https://www.wu.ac.at/)
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <nshttp2/utility/memory.h>
#include <string.h>
#include <pthread.h>
#include <nshttp2/stddef.h>
#include <nshttp2/utility/nghttp2.h>

static pthread_mutex_t allocatorMutex = PTHREAD_MUTEX_INITIALIZER;

#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L

static Http2MemoryAllocatorsT http2MemoryAllocators = {
        .malloc = malloc,
        .calloc = calloc,
        .realloc = realloc,
        .free = free
};

#else

static Http2MemoryAllocatorsT http2MemoryAllocators;

#endif

void *http2Malloc(const size_t size) {
    return http2MemoryAllocators.malloc(size);
}

void *http2Calloc(const size_t size) {
    return http2MemoryAllocators.calloc(1u, size);
}

void http2Free(void *const ptr) {
    http2MemoryAllocators.free(ptr);
}

void *http2Realloc(void *const ptr, const size_t size) {
    return http2MemoryAllocators.realloc(ptr, size);
}

static void *http2MallocDefault(size_t nmemb, void *UNUSED(userData)) {
    return http2Malloc(nmemb);
}

static void *http2CallocDefault(size_t nmemb, size_t size, void *UNUSED(userData)) {
    return http2Calloc(nmemb * size);
}

static void http2FreeDefault(void *ptr, void *UNUSED(memUserData)) {
    http2Free(ptr);
}

static void *http2ReallocDefault(void *ptr, size_t size, void *UNUSED(memUserData)) {
    return http2Realloc(ptr, size);
}

void http2SetupMemoryAllocators(Http2MemoryAllocatorsT *HTTP2_CONST_RESTRICT_SPECIFIER memoryAllocators) {
    pthread_mutex_lock(&allocatorMutex);

    if (memoryAllocators == NULL) {
        http2MemoryAllocators.realloc = realloc;
        http2MemoryAllocators.free = free;
        http2MemoryAllocators.calloc = calloc;
        http2MemoryAllocators.malloc = malloc;
    } else {
        nghttp2_mem nghttp2Mem;
        nghttp2Mem.malloc = http2MallocDefault;
        nghttp2Mem.mem_user_data = NULL;
        nghttp2Mem.calloc = http2CallocDefault;
        nghttp2Mem.free = http2FreeDefault;
        nghttp2Mem.realloc = http2ReallocDefault;

        http2MemoryAllocators = *memoryAllocators;
        ngHttp2SetupMemoryAllocators(&nghttp2Mem);
    }

    pthread_mutex_unlock(&allocatorMutex);
}
